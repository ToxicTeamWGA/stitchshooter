﻿using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using Pathfinding.Serialization;
using System.Linq;
using System;

namespace Pathfinding
{
    [JsonOptIn]
    [Pathfinding.Util.Preserve]
    public class ReachGraph : NavGraph
    {
        [JsonMember]
        public Vector2 center = Vector2.zero;
        [JsonMember]
        public float scale = 0.3f; //how much above hit point a graph point shall be
        [JsonMember]
        public int steps = 360; //how many steps with a distance shall be made
        [JsonMember]
        public float distance = 0.7f; //distance between waypoints
        [JsonMember]
        public float maxJumpHeight = 4;
        [JsonMember]
        public int nodesLookBack = 7;

        PointNode[] nodes;
        List<PointNode> groundNodes;
        List<PointNode> platformNodes;



        private float NodesYDiff(PointNode a, PointNode b)
        {
            if (a.position.y > b.position.y) return a.position.y - b.position.y;
            else return b.position.y - a.position.y;
        }

        private float NodesXDiff(PointNode a, PointNode b)
        {
            if (a.position.x > b.position.x) return a.position.x - b.position.x;
            else return b.position.x - a.position.x;
        }

        private Vector3 NodesPos(PointNode a)
        {
            return new Vector3(a.position.x / Int3.FloatPrecision, a.position.y / Int3.FloatPrecision, 0);
        }

        private float NodesDist(PointNode a, PointNode b)
        {
            return Vector2.Distance(Node2Vector(a), Node2Vector(b));
        }

        private Vector2 Node2Vector(PointNode a)
        {
            return new Vector2(a.position.x / Int3.FloatPrecision, a.position.y / Int3.FloatPrecision);
        }

        private void addConnection(PointNode from, PointNode to, float multiplier = 1f)
        {
            List<Connection> connections;
            if (from.connections == null)
                connections = new List<Connection>();
            else connections = new List<Connection>(from.connections);

            connections.Add(new Connection(to, (uint)Mathf.Round(multiplier * NodesDist(from, to))));
            from.connections = connections.ToArray();
        }

        protected override IEnumerable<Progress> ScanInternal()
        {
            groundNodes = new List<PointNode>();
            platformNodes = new List<PointNode>();
            //int nodesLookBack = (int)Math.Round(maxJumpHeight / distance);

            groundNodes.Add(CreateNode(new Vector3(center.x, center.y, 0)));
            PointNode curNode;
            PointNode prevNode;
            float ydiff;
            int underId = 0;
            List<Connection> cons;
            float precisedDistance = distance * Int3.FloatPrecision;
            for (int i = 1; i < steps; i++)
            {
                Vector3 pos = new Vector3(center.x + i * distance, center.y, 0);
                Debug.DrawRay(new Vector3(center.x + i * distance, 0, 0), Vector2.up, Color.red, 3);
                RaycastHit2D[] hits = Physics2D.RaycastAll(pos, Vector2.down, 30, LayerMask.GetMask("Ground", "Platform"));

                foreach (RaycastHit2D hit in hits)
                {

                    if (hit.collider != null)
                    {
                        pos.y = hit.point.y + scale;
                    }
                    else pos = Vector3.zero;

                    if (hit.collider.gameObject.CompareTag("GroundMain"))
                    {
                        groundNodes.Add(CreateNode(pos));
                        curNode = groundNodes[groundNodes.Count - 1];
                        for (int count = groundNodes.Count - nodesLookBack; count < groundNodes.Count - 1 && count >= 0; count++)
                        {
                            prevNode = groundNodes[count];
                            ydiff = NodesYDiff(curNode, prevNode);
                            if (ydiff < maxJumpHeight * Int3.FloatPrecision
                                && ydiff > Int3.FloatPrecision / 4
                                && (!Physics2D.Raycast(Node2Vector(prevNode), (Node2Vector(curNode) - Node2Vector(prevNode)).normalized * NodesDist(curNode, prevNode), LayerMask.NameToLayer("Ground"))
                                || !Physics2D.Raycast(Node2Vector(curNode), (Node2Vector(prevNode) - Node2Vector(curNode)).normalized * NodesDist(prevNode, curNode), LayerMask.NameToLayer("Ground")))
                                )
                            {
                                addConnection(prevNode, curNode);
                                addConnection(curNode, prevNode);
                            }
                            else if (ydiff < maxJumpHeight * Int3.FloatPrecision
                              && ydiff < Int3.FloatPrecision / 4)
                            {
                                addConnection(prevNode, curNode);
                                addConnection(curNode, prevNode);
                            }

                            if (prevNode.position.y > curNode.position.y
                                && !Physics2D.Raycast(Node2Vector(curNode), (Node2Vector(prevNode) - Node2Vector(curNode)).normalized * NodesDist(prevNode, curNode), LayerMask.NameToLayer("Ground")))
                                addConnection(prevNode, curNode);
                        }
                    }
                    else if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Platform"))
                    {
                        platformNodes.Add(CreateNode(pos));
                        if (platformNodes.Count > 1)
                        {
                            curNode = platformNodes[platformNodes.Count - 1];
                            prevNode = platformNodes[platformNodes.Count - 2];
                            ydiff = NodesYDiff(curNode, prevNode);
                            if (ydiff < maxJumpHeight * 100 && NodesXDiff(curNode, prevNode) <= precisedDistance)
                            {
                                addConnection(prevNode, curNode);
                                addConnection(curNode, prevNode);
                            }
                            //if (ydiff > 1000) Debug.DrawLine((Vector3)prevNode.position, (Vector3)curNode.position, Color.green, 10);
                        }
                    }
                }
            }

            for (int j = 0; j < platformNodes.Count; j++)
            {
                PointNode plNode = platformNodes[j];
                underId = (int)Math.Round((plNode.position.x - center.x * Int3.FloatPrecision) / (precisedDistance));
                for (int count = underId - nodesLookBack; count < underId + nodesLookBack + 1 && count >= 0 && count < groundNodes.Count - nodesLookBack; count++)
                {
                    PointNode underNode = groundNodes[count];
                    addConnection(plNode, underNode);
                    if (NodesYDiff(plNode, underNode) < maxJumpHeight * Int3.FloatPrecision
                               && NodesYDiff(plNode, underNode) > Int3.FloatPrecision / 4)
                        addConnection(underNode, plNode, 5);
                }

                for (int k = j - nodesLookBack; k >= 0 && k <= platformNodes.Count - nodesLookBack; k++)
                {
                    if (NodesYDiff(plNode, platformNodes[k]) < maxJumpHeight * Int3.FloatPrecision
                                && NodesXDiff(plNode, platformNodes[k]) < 3500)
                    {
                        addConnection(plNode, platformNodes[k]);
                        addConnection(platformNodes[k], plNode);
                    }
                    if (NodesXDiff(plNode, platformNodes[k]) < maxJumpHeight * 2000
                        && NodesYDiff(plNode, platformNodes[k]) < maxJumpHeight * 200)
                    {
                        addConnection(plNode, platformNodes[k]);
                        addConnection(platformNodes[k], plNode);
                    }
                }
            }

            groundNodes.AddRange(platformNodes);
            nodes = groundNodes.ToArray();
            for (int i = 0; i < nodes.Length; i++)
            {
                nodes[i].Walkable = true;
            }
            yield break;
        }

        public override void GetNodes(System.Action<GraphNode> action)
        {
            if (nodes == null) return;
            for (int i = 0; i < nodes.Length; i++)
            {
                action(nodes[i]);
            }
        }

        PointNode CreateNode(Vector3 position)
        {
            var node = new PointNode(active);
            node.position = (Int3)position;
            return node;
        }
    }
}