﻿using UnityEditor;
using Pathfinding;
using UnityEngine;

namespace Pathfinding
{
    [CustomGraphEditor(typeof(ReachGraph), "ReachGraph")]
    public class ReachGraphEditor : GraphEditor
    {
        public override void OnInspectorGUI(NavGraph target)
        {
            var graph = target as ReachGraph;

            graph.steps = EditorGUILayout.IntField("Steps", graph.steps);
            graph.nodesLookBack = EditorGUILayout.IntField("Nodes Look Back", graph.nodesLookBack);
            graph.distance = EditorGUILayout.FloatField("Distance", graph.distance);
            graph.scale = EditorGUILayout.FloatField("Scale", graph.scale);
            graph.center = EditorGUILayout.Vector2Field("Center", graph.center);
            graph.maxJumpHeight = EditorGUILayout.FloatField("Max Jump Height", graph.maxJumpHeight);
        }
    }
}