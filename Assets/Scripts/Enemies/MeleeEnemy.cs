using ScriptableObjects.Config.Enemy;
using UnityEngine;

namespace Enemies {
    public class MeleeEnemy<T> : Enemy<T> where T : EnemyConfig {
        public Animator animator;
        private static readonly int horizontalSpeed = Animator.StringToHash("HorizontalSpeed");
        private static readonly int isDead = Animator.StringToHash("IsDead");

        protected override void Attack() {
            //TODO: play attack animation and hit player
        }

        protected override void Move() {
            rb.velocity = new Vector2(
                targetPosition.value.x > transform.position.x ? config.baseSpeed : -config.baseSpeed,
                rb.velocity.y);
        }
    }
}