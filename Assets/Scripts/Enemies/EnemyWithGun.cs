using ScriptableObjects.Config.Enemy;
using SFX;
using Shooting;
using Shooting.Weapons;
using UnityEngine;
using Utils;
using Random = UnityEngine.Random;

namespace Enemies
{
    public class EnemyWithGun : Enemy<EnemyWithGunConfig>, IMovementConfuser
    {
        public Weapon gun;
        public RotateToPointer rotateToPointer;
        public bool showAggroRadius = true;
        public GameObject hands;
        public GameObject rightHand;
        public float moveRange = 0.3f;
        public Animator animator;
        public float waitTime = 1f;
        public float prepareTime;
        public float accelerationTime = 1;
        private float _attackTimer;
        private float _randomTime;
        private Vector2 rightPoint;
        private Vector2 leftPoint;
        private float _waitTimer;
        private float _prepareTimer;
        private float _accelerationTimer;
        private bool needTurn;
        private static readonly int hSpeed = Animator.StringToHash("HSpeed");
        private static readonly int aggro = Animator.StringToHash("Aggro");

        private void Awake()
        {
            _attackTimer = config.attackCoolDown;
            rotateToPointer.enabled = false;
            _randomTime = Random.Range(config.attackRandomMin, config.attackRandomMax);
            gun.Init(this);
            hands.SetActive(false);
            rightHand.SetActive(false);
            _waitTimer = waitTime;
            var transformPosition = transform.position;
            leftPoint = new Vector2(transformPosition.x - moveRange, transformPosition.y);
            rightPoint = new Vector2(transformPosition.x + moveRange, transformPosition.y);
        }

        public override void Init()
        {
            base.Init();
            _attackTimer = config.attackCoolDown;
        }

        public override void Dead()
        {
            base.Dead();
            VoiceAudioController.Instance.RandomPhrase();
        }

        protected override void Attack()
        {
            gun.Shoot();
        }

        protected override void Move()
        {
            _waitTimer += Time.deltaTime;
            if (_waitTimer >= waitTime)
            {
                _accelerationTimer += Time.deltaTime;
                if (needTurn)
                {
                    TurnAround();
                    _accelerationTimer = 0;
                    needTurn = false;
                }

                var transformLocalScale = transform.localScale;
                var transformPosition = transform.position;
                if (transformLocalScale.x > 0)
                {
                    if (rightPoint.x - transformPosition.x < 0.1)
                    {
                        needTurn = true;
                        _waitTimer = 0;
                    }
                }
                else
                {
                    if (transformPosition.x - leftPoint.x < 0.1)
                    {
                        needTurn = true;
                        _waitTimer = 0;
                    }
                }

                rb.velocity = Vector2.Lerp(rb.velocity, config.baseSpeed * transform.localScale.x * Vector2.right,
                    _accelerationTimer / accelerationTime);
                animator.SetFloat(hSpeed, Mathf.Abs(rb.velocity.x));
            }

            animator.SetFloat(hSpeed, Mathf.Abs(rb.velocity.x));
        }

        private void TurnAround()
        {
            var transformLocalScale = transform.localScale;
            transformLocalScale.x *= -1;
            transform.localScale = transformLocalScale;
        }

        public void ShootConfuse(float time, float movementSlow)
        {
        }

        private void FixedUpdate()
        {
            if (!isAlive) return;
            var checkAggro = CheckAggro();
            if (checkAggro)
            {
                _prepareTimer += Time.deltaTime;
                if (transform.localScale.x < 0)
                {
                    TurnAround();
                }

                animator.SetBool(aggro, true);
                rotateToPointer.enabled = true;
                _attackTimer += Time.deltaTime;
                if (_prepareTimer >= prepareTime)
                {
                    hands.SetActive(true);
                    rightHand.SetActive(true);
                }

                if (_attackTimer >= (config.attackCoolDown + _randomTime) && _prepareTimer >= prepareTime)
                {
                    _attackTimer = 0;
                    _randomTime = Random.Range(config.attackRandomMin, config.attackRandomMax);
                    if (CanAttack())
                    {
                        Attack();
                    }
                }
            }
            else if (moveRange > 0.01)
            {
                Move();
            }
        }

        protected override bool CanAttack()
        {
            return base.CanAttack() && CheckAggro();
        }

        private void OnDrawGizmos()
        {
            if (showAggroRadius)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(transform.position, aggroDistance);
            }
        }
    }
}