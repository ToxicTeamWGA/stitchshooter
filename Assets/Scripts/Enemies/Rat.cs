using Pathfinding;
using SaveSystem;
using ScriptableObjects.Config.Enemy;
using SFX;
using UnityEngine;
using Utils;

namespace Enemies
{
    public class Rat : MeleeEnemy<RatConfig>
    {
        public RatAudioController audioPlayer;
        public float deltaDistanceToJump = 0.2f;
        public Throwable[] throwable;
        public GameObject danger;
        public GameObject claws;
        private Path path;
        private int currentWaypoint;
        private Seeker seeker;
        private float attackCooldownTimer;
        private float _prepareTimer;
        private float speed;
        private RatState _state = RatState.Move;
        private static readonly int hSpeed = Animator.StringToHash("HSpeed");
        private static readonly int jumpAttack = Animator.StringToHash("JumpAttack");
        private static readonly int death = Animator.StringToHash("Death");
        private static readonly int grounded = Animator.StringToHash("Grounded");

        protected override void Start()
        {
            base.Start();
            seeker = GetComponent<Seeker>();
            InvokeRepeating(nameof(UpdatePath), 0f, .3f);
            attackCooldownTimer = config.attackCoolDown;
            speed = config.baseSpeed + Random.Range(-config.speedRandomBounds, config.speedRandomBounds);
            danger.SetActive(false);
        }

        public override void Dead()
        {
            base.Dead();
            PersistenceManager.Instance.statisticsManager.IncKilled();
            audioPlayer.Dead();
            var range = Random.Range(1, 4);
            animator.SetInteger(death, range);
            throwable[range - 1].gameObject.SetActive(true);
            throwable[range - 1].Throw();


            CancelInvoke(nameof(UpdatePath));
            seeker.CancelCurrentPathRequest();
            seeker.enabled = false;
            danger.SetActive(false);
            claws.SetActive(false);
            VoiceAudioController.Instance.RandomPhrase();
        }

        private void RotateToPlayer()
        {
            var transformLocalScale = transform.localScale;
            transformLocalScale.x = rb.position.x < targetPosition.value.x ? 1 : -1;
            transform.localScale = transformLocalScale;
        }

        void FixedUpdate()
        {
            if (isAlive && CheckAggro())
            {
                audioPlayer.Run(false);
                attackCooldownTimer += Time.deltaTime;
                switch (_state)
                {
                    case RatState.Move:
                        if (IsGrounded())
                        {
                            animator.SetBool(grounded, true);
                            claws.SetActive(false);
                            if (CanAttack())
                            {
                                Prepare();
                            }
                            else
                            {
                                Move();
                            }
                        }
                        else
                        {
                            animator.SetBool(grounded, false);
                        }

                        break;
                    case RatState.Prepare:
                        _prepareTimer += Time.deltaTime;
                        if (_prepareTimer > config.prepareTime)
                        {
                            danger.SetActive(false);
                            _prepareTimer = 0;
                            Attack();
                            _state = RatState.Move;
                        }

                        break;
                    case RatState.Attack:
                        break;
                }
            }

            animator.SetFloat(hSpeed, Mathf.Abs(rb.velocity.x));
        }

        //updating path
        void UpdatePath()
        {
            if (seeker.IsDone() && (rb.position.x > 0 || rb.position.x < 0))
                seeker.StartPath(rb.position, targetPosition.value, OnPathComplete);
        }

        //finish calculating path
        void OnPathComplete(Path p)
        {
            if (!p.error)
            {
                path = p;
                currentWaypoint = 0;
            }
        }

        protected override bool CanAttack()
        {
            return base.CanAttack() && attackCooldownTimer > config.attackCoolDown;
        }

        protected override void Attack()
        {
            attackCooldownTimer = 0;
            audioPlayer.Bite();
            claws.SetActive(true);
            Leap();
        }

        private void Prepare()
        {
            _state = RatState.Prepare;
            _prepareTimer = 0;
            animator.SetTrigger(jumpAttack);
            RotateToPlayer();
            danger.SetActive(true);
        }

        //apply leap in a certain direction with a certain force
        private void Leap()
        {
            var direction = transform.localScale.x > 0
                ? new Vector2(0.5f, 0.3f)
                : new Vector2(-0.5f, 0.3f);
            rb.AddForce(config.leapForce * direction, ForceMode2D.Impulse);
        }

        private Vector2 GetFollowPoint()
        {
            var currentPoint = path.vectorPath[currentWaypoint];
            var deltaX = Mathf.Abs(rb.position.x - currentPoint.x);
            var deltaY = Mathf.Abs(rb.position.y - currentPoint.y);

            if (deltaX < config.nextWaypointDistMax && deltaY < deltaDistanceToJump)
            {
                if (currentWaypoint < path.vectorPath.Count - 1)
                {
                    currentWaypoint++;
                    return path.vectorPath[currentWaypoint];
                }
            }

            if (path.vectorPath.Count > currentWaypoint + 1)
            {
                var nextPoint = path.vectorPath[currentWaypoint + 1];
                if (Vector2.Distance(nextPoint, rb.position) < Vector2.Distance(nextPoint, currentPoint))
                {
                    currentWaypoint++;
                    return nextPoint;
                }
            }

            return currentPoint;
        }

        //moving and pathfinding
        protected override void Move()
        {
            if (path == null) return;
            audioPlayer.Run(true);
            var followPoint = GetFollowPoint();

            var direction = (followPoint - rb.position);
            var moveDirection = direction.normalized;
            Jump(followPoint);
            var transformLocalScale = transform.localScale;
            transformLocalScale.x = moveDirection.x > 0 ? 1 : -1;
            transform.localScale = transformLocalScale;
        }

        private void Jump(Vector3 targetPosition)
        {
            var currentPosition = transform.position;
            var g = Physics.gravity.magnitude * rb.gravityScale;

            var planarTarget = new Vector3(targetPosition.x, 0, targetPosition.z);
            var planarPosition = new Vector3(currentPosition.x, 0, currentPosition.z);

            var axis = targetPosition.x > currentPosition.x ? Vector3.right : Vector3.left;

            var angleToJump = Mathf.Clamp(Vector3.SignedAngle(axis, targetPosition - currentPosition, axis) + 15, 20,
                75);
            var angle = angleToJump * Mathf.Deg2Rad;
            var distance = Vector3.Distance(planarTarget, planarPosition);
            var yOffset = currentPosition.y - targetPosition.y;

            var initialVelocity = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * g * Mathf.Pow(distance, 2)) /
                                                                      (distance * Mathf.Tan(angle) + yOffset));

            var velocity = new Vector3(0, initialVelocity * Mathf.Sin(angle), initialVelocity * Mathf.Cos(angle));

            var angleBetweenObjects = Vector3.SignedAngle(Vector3.forward, planarTarget - planarPosition, Vector3.up);
            var finalVelocity = Quaternion.AngleAxis(angleBetweenObjects, Vector3.up) * velocity;

            if (float.IsNaN(finalVelocity.x) || float.IsNaN(finalVelocity.y))
            {
                rb.velocity = Vector2.right * speed;
                return;
            }

            finalVelocity.z = 0;
            if (Mathf.Abs(targetPosition.y - rb.position.y) >= deltaDistanceToJump ||
                Physics2D.Raycast(transform.position, Vector2.down,
                    config.groundDistance * 2f, config.groundLayerMask).collider == null)
            {
                rb.velocity = Vector2.ClampMagnitude(finalVelocity, 30f);
            }
            else if (((Vector2) targetPosition - rb.position).magnitude < 1.5f)
            {
                finalVelocity.y = 0;
                rb.velocity = finalVelocity;
            }
            else
            {
                rb.velocity = Vector2.ClampMagnitude(finalVelocity, speed);
            }
        }

        private enum RatState
        {
            Move,
            Prepare,
            Attack
        }
    }
}