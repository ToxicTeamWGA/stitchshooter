using System;
using Attributes;
using ScriptableObjects.Config.Enemy;
using ScriptableObjects.Variables;
using UnityEngine;
using Utils;

namespace Enemies
{
    [RequireComponent(typeof(Rigidbody2D))]
    [Serializable]
    public abstract class Enemy<T> : PoolObject where T : EnemyConfig
    {
        [Expandable] public T config;
        public Vector3Variable targetPosition;
        public Collider2D bodyCollider;
        public float aggroDistance;
        public bool noiseAggro;
        public LayerMask aggroRaycastLayerMask;
        public LayerMask playerLayer;
        public BoolVariable isPlayerAlive;
        public ParticleSystem bloodToScreen;
        public Transform bloodThrow;
        protected Rigidbody2D rb;
        protected bool isAlive = true;
        protected float stunDuration;
        protected bool isAggro;
        protected Transform cameraTransform;
        private readonly RaycastHit2D[] _hits = new RaycastHit2D[1];
        private bool needBloodThrowRecalculation;

        protected abstract void Attack();
        protected abstract void Move();

        protected virtual void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            rb.freezeRotation = true;
            cameraTransform = Camera.main.transform;
            needBloodThrowRecalculation = bloodThrow != null && bloodToScreen != null;
            if (needBloodThrowRecalculation)
            {
                var mainModule = bloodToScreen.main;
                mainModule.customSimulationSpace = cameraTransform;
            }
        }

        public void RecalculateBloodAngle()
        {
            if (!needBloodThrowRecalculation) return;
            var bloodThrowEulerAngles = bloodThrow.eulerAngles;
            var y = cameraTransform.position.y - bloodThrow.position.y;
            var z = Mathf.Abs(cameraTransform.position.z - bloodThrow.position.z);
            bloodThrowEulerAngles.x = Mathf.Tan(y / z) * Mathf.Rad2Deg;
            bloodThrow.eulerAngles = bloodThrowEulerAngles;
        }

        private void Update()
        {
            RecalculateBloodAngle();
        }

        public virtual void Dead()
        {
            isAlive = false;
            bodyCollider.gameObject.layer = LayerMask.NameToLayer("Dead");
            RecalculateBloodAngle();
        }

        public virtual void Stun(float duration)
        {
            stunDuration = duration;
        }

        protected bool CheckAggro()
        {
            if (!isPlayerAlive.value)
            {
                return false;
            }

            if (!isAggro)
            {
                if (noiseAggro)
                {
                    isAggro = CheckNoise();
                }
                else
                {
                    isAggro = CheckVisibility();
                }
            }

            return isAggro;
        }

        protected bool CheckVisibility()
        {
            var transformPosition = transform.position;
            var raycastHit2D = Physics2D.Raycast(transformPosition, targetPosition.value - transformPosition,
                aggroDistance, aggroRaycastLayerMask);
            return raycastHit2D && CollisionUtils.MaskCheck(raycastHit2D.collider.gameObject.layer, playerLayer);
        }

        protected bool CheckNoise()
        {
            return Physics2D.OverlapCircle(transform.position, aggroDistance, playerLayer);
        }

        protected virtual bool CanAttack()
        {
            var heading = targetPosition.value - transform.position;
            return heading.sqrMagnitude < config.attackRange * config.attackRange;
        }

        protected bool IsGrounded()
        {
            return Physics2D.BoxCastNonAlloc(transform.position + 0.5f * config.groundDistance * Vector3.up,
                       bodyCollider.bounds.size, 0, Vector3.down,
                       _hits, config.groundDistance, config.groundLayerMask) > 0;
        }
    }
}