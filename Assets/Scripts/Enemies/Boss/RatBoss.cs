﻿using Health;
using ScriptableObjects.Config.Enemy;
using SFX;
using UnityEngine;
using Utils.Camera;
using SaveSystem;

namespace Enemies.Boss
{
    public class RatBoss : Enemy<RatBossConfig>, IDamager
    {
        public Transform raycastPoint;
        public LayerMask wallsLayerMask;
        public LayerMask playerLayerMask;
        public Animator animator;
        public int searchDistance = 10;
        public float stunTime = 3f;
        public float attackSpeed;
        public float waitTimeAfterDamage;
        public float minDistanceToWall;
        public float pushForce;
        public float pushAngle;
        public float prepareTime;
        public BossAudioController audioPlayer;
        public Shake screenShaker;
        public float shakeTime;
        public float Damage => damage;
        protected float damage;

        public bool shouldCircleOverlap;

        private bool _isFlipped;
        private BossState _state;
        private float _currentTimeAfterDamage;
        private float _shakeTimer;
        private float _prepareTimer;

        private readonly int attackAnimation = Animator.StringToHash("Attack");
        private readonly int waiting = Animator.StringToHash("Waiting");
        private readonly int damageWait = Animator.StringToHash("DamageWait");
        private static readonly int hSpeed = Animator.StringToHash("HSpeed");
        private static readonly int dash = Animator.StringToHash("dash");
        private int hp = 3;
        private static readonly int death = Animator.StringToHash("Death");

        private void LookAtPlayer()
        {
            var flipped = transform.localScale;
            flipped.z *= -1f;
            if (transform.position.x < targetPosition.value.x && _isFlipped)
            {
                transform.localScale = flipped;
                transform.Rotate(0f, 180f, 0f);
                _isFlipped = false;
            }
            else if (transform.position.x > targetPosition.value.x && !_isFlipped)
            {
                transform.localScale = flipped;
                transform.Rotate(0f, 180f, 0f);
                _isFlipped = true;
            }
        }

        protected override void Start()
        {
            base.Start();
            _isFlipped = false;
            _state = BossState.Patrolling;
            _currentTimeAfterDamage = 0;
            damage = config.damage;
            isAlive = false;
            _shakeTimer = shakeTime;
        }

        public void SetAlive()
        {
            isAlive = true;
            audioPlayer.Run(true);
        }

        protected void FixedUpdate()
        {
            if (!isAlive) return;
            switch (_state)
            {
                case BossState.Patrolling:
                    Patrol();
                    break;
                case BossState.Follow:
                    LookAtPlayer();
                    if (CanAttack()) Prepare();
                    else Move();
                    break;
                case BossState.AttackRun:
                    CameraShake();
                    CheckWallSmash();
                    break;
                case BossState.Prepare:
                    _prepareTimer += Time.deltaTime;
                    if (_prepareTimer > prepareTime)
                    {
                        Attack();
                    }

                    break;
                case BossState.Stunned:
                    stunDuration -= Time.deltaTime;
                    if (stunDuration <= 0)
                    {
                        _state = BossState.Patrolling;
                        // animator.SetBool(waiting, false);
                        audioPlayer.Run(true);
                    }

                    break;
                case BossState.Damaged:
                    _currentTimeAfterDamage -= Time.deltaTime;
                    if (_currentTimeAfterDamage <= 0)
                    {
                        _state = BossState.Patrolling;
                        animator.SetBool(damageWait, false);
                        audioPlayer.Run(true);
                    }

                    break;
            }

            animator.SetFloat(hSpeed, Mathf.Abs(rb.velocity.x));
        }

        private void CameraShake()
        {
            _shakeTimer += Time.deltaTime;
            if (_shakeTimer >= shakeTime)
            {
                _shakeTimer = 0;
                screenShaker.Play(Vector2.up);
            }
        }

        protected override void Attack()
        {
            _state = BossState.AttackRun;
            audioPlayer.AttackRun();
            RunToWall();
        }

        private void Prepare()
        {
            animator.SetTrigger(dash);
            _state = BossState.Prepare;
            _prepareTimer = 0;
            rb.velocity = Vector2.zero;
        }

        public void DamageState()
        {
            hp--;
            switch (hp)
            {
                case 2:
                    VoiceAudioController.Instance.BossFirstHarm(1.5f);
                    break;
                case 1:
                    VoiceAudioController.Instance.BossSecondHarm(1.5f);
                    break;
            }

            _state = BossState.Damaged;
            _currentTimeAfterDamage = waitTimeAfterDamage;
            if (hp > 0)
            {
                animator.SetBool(damageWait, true);
                audioPlayer.Harm();
            }

            rb.velocity = Vector2.zero;
        }

        protected override void Move()
        {
            var direction = new Vector2((targetPosition.value - transform.position).normalized.x, 0);
            rb.velocity = direction * config.baseSpeed;
        }

        private void Patrol()
        {
            if (IsWallTouched())
            {
                Flip();
            }

            CheckPlayer();
            rb.velocity = new Vector2(_isFlipped ? -1 : 1, 0) * config.baseSpeed;
        }

        private void CheckPlayer()
        {
            bool playerFound;
            if (shouldCircleOverlap)
            {
                playerFound = Physics2D.OverlapCircle(transform.position, searchDistance, playerLayerMask);
            }
            else
            {
                playerFound = Physics2D.Raycast(raycastPoint.position,
                    new Vector2(_isFlipped ? -minDistanceToWall : minDistanceToWall, 0),
                    searchDistance, playerLayerMask);
            }

            _state = playerFound ? BossState.Follow : BossState.Patrolling;
        }

        private bool IsWallTouched()
        {
            return Physics2D.Raycast(raycastPoint.position,
                new Vector2(_isFlipped ? -1 : 1, 0), minDistanceToWall,
                wallsLayerMask);
        }

        private void Flip()
        {
            var flipped = transform.localScale;
            flipped.z *= -1f;
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            _isFlipped = !_isFlipped;
        }

        private void CheckWallSmash()
        {
            if (IsWallTouched())
            {
                _state = BossState.Stunned;
                //animator.SetBool(waiting, true);
                stunDuration = stunTime;
                audioPlayer.Stun();
                audioPlayer.Run(false);
                rb.velocity = Vector2.zero;
            }
            else
            {
                RunToWall();
            }
        }

        private void RunToWall()
        {
            rb.velocity = new Vector2(_isFlipped ? -attackSpeed : attackSpeed, 0);
        }

        public void Push(Collider2D other)
        {
            var force = Quaternion.AngleAxis(pushAngle, _isFlipped ? Vector3.back : Vector3.forward) *
                        rb.velocity.normalized * pushForce;
            var otherAttachedRigidbody = other.attachedRigidbody;
            otherAttachedRigidbody.GetComponent<Player>().SpendHP(damage);
            otherAttachedRigidbody.velocity = force;
        }

        public override void Dead()
        {
            base.Dead();
            PersistenceManager.Instance.statisticsManager.IncKilled();
            rb.velocity = Vector2.zero;
            audioPlayer.Dead();
            gameObject.layer = LayerMask.NameToLayer("Ground");
            animator.SetTrigger(death);
        }


        private void OnDrawGizmos()
        {
            if (shouldCircleOverlap) Gizmos.DrawWireSphere(transform.position, searchDistance);
        }

        private enum BossState
        {
            Patrolling,
            Follow,
            AttackRun,
            Prepare,
            Stunned,
            Damaged
        }
    }
}