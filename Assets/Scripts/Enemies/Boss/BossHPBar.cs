﻿using Health;
using UnityEngine;
using UnityEngine.UI;

namespace Enemies.Boss {
    public class BossHPBar : MonoBehaviour {
        public Image fill;
        public HealthComponent health;
        public float loweringDelta = 0.05f;
        private float maxHealth;
        private bool isLowering;

        public void Start() {
            maxHealth = health.MaxHealth;
            fill.fillAmount = 1;
            gameObject.SetActive(false);
        }

        public void ShowHpBar() {
            gameObject.SetActive(true);
        }

        public void UpdateValue() {
            isLowering = true;
        }

        private void Update() {
            if (!isLowering) return;
            fill.fillAmount = Mathf.MoveTowards(fill.fillAmount, health.Health / maxHealth, loweringDelta);
            if (fill.fillAmount.Equals(health.Health / maxHealth)) isLowering = false;
        }

        public void HideHpBar() {
            gameObject.SetActive(false);
        }
    }
}