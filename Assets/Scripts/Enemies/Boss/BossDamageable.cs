﻿using Health;
using UnityEngine;
using Utils;

namespace Enemies.Boss {
    public class BossDamageable : Damageable {
        public Animator animator;
        private bool triggered;
        private static readonly int harm = Animator.StringToHash("harm");

        private void OnTriggerEnter2D(Collider2D other) {
            if (triggered) return;
            if (CollisionUtils.MaskCheck(other.gameObject.layer, layerMask) && healthComponent.Health > 0) {
                triggered = true;
                animator.SetTrigger(harm);
                var damage = other.gameObject.GetComponent<IDamager>().Damage * damageMultiplier;
                if (damage > 0) {
                    healthComponent.Health -= damage;
                }
            }
        }

        private void OnTriggerExit2D(Collider2D other) {
            if (CollisionUtils.MaskCheck(other.gameObject.layer, layerMask)) triggered = false;
        }
    }
}