using Health;
using UnityEngine;

namespace Enemies {
    public class RatDamager : MonoBehaviour, IDamager {
        public float damage;

        public float Damage => damage;
    }
}