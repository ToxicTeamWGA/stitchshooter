﻿using UnityEngine;

namespace VFX {
    public class BloodSplash : MonoBehaviour {
        public bool randomRotation = false;
        private ParticleSystem particles;

        void Start() {
            particles = GetComponentInChildren<ParticleSystem>();
        }

        public void Splash(Vector3 position) {
            transform.position = position;
            if (randomRotation) {
                transform.eulerAngles = new Vector3(0, 0, Random.Range(-25, 25));
            }

            particles.Play();
        }
    }
}