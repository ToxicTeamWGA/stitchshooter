using System;
using UnityEngine;
using UnityEngine.Events;

namespace Events {
    [Serializable]
    public class ColliderEvent : UnityEvent<Collider2D> {
    }
}