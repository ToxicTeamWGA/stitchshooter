using UnityEngine;
using Utils;

namespace Events {
    [RequireComponent(typeof(Collider2D))]
    public class ColliderTrigger : MonoBehaviour {
        public LayerMask triggerLayers;
        public ColliderEvent onTrigger;

        private void OnTriggerEnter2D(Collider2D other) {
            if (!CollisionUtils.MaskCheck(other.gameObject.layer, triggerLayers)) return;
            onTrigger.Invoke(other);
        }
    }
}