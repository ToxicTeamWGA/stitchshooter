using System;
using UnityEngine;
using UnityEngine.Events;

namespace Events {
    [Serializable]
    public class Vector3Event : UnityEvent<Vector3> {
    }
}