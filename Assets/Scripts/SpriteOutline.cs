﻿using UnityEngine;

//[ExecuteInEditMode]
public class SpriteOutline : MonoBehaviour {
    public Color color = Color.white;

    [Range(0, 16)] public int outlineSize = 1;
    public float blinkTime = 1;

    private SpriteRenderer _spriteRenderer;
    private float _time;
    private static readonly int outlineProperty = Shader.PropertyToID("_Outline");
    private static readonly int outlineColor = Shader.PropertyToID("_OutlineColor");
    private static readonly int size = Shader.PropertyToID("_OutlineSize");

    void OnEnable() {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        UpdateOutline(true);
    }

    void OnDisable() {
        UpdateOutline(false);
    }

    void Update() {
        UpdateOutline(true);
    }

    void UpdateOutline(bool outline) {
        _time += Time.deltaTime;
        color.a = Mathf.Lerp(1, 0, _time / blinkTime);
        if (_time >= blinkTime) {
            _time = 0;
        }

        MaterialPropertyBlock mpb = new MaterialPropertyBlock();
        _spriteRenderer.GetPropertyBlock(mpb);
        mpb.SetFloat(outlineProperty, outline ? 1f : 0);
        mpb.SetColor(outlineColor, color);
        mpb.SetFloat(size, outlineSize);
        _spriteRenderer.SetPropertyBlock(mpb);
    }
}