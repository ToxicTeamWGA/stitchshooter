using ScriptableObjects.Variables;
using UnityEngine;

public class Pointer : MonoBehaviour {
    public Vector3Variable positionContainer;
    public float canvasPlaneDistance = 10f;
    public Camera mainCamera;
    public SpriteRenderer sprite;

    private void Awake() {
        Vector3 pointer = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        pointer.z = canvasPlaneDistance;
        positionContainer.value = pointer;
    }

    private void Update() {
        if (Time.timeScale > 0) {
            SetAlpha(1);
            var screenPoint = Input.mousePosition;
            screenPoint.z = canvasPlaneDistance;
            var screenToWorldPoint = mainCamera.ScreenToWorldPoint(screenPoint);
            transform.position = screenToWorldPoint;
            positionContainer.value = screenToWorldPoint;
        }
        else {
            SetAlpha(.01f);
        }
    }
    
    private void SetAlpha(float a) {
        var panelColor = sprite.color;
        panelColor.a = a;
        sprite.color = panelColor;
    }
}