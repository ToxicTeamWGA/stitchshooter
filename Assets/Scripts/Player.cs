using Attributes;
using Events;
using ScriptableObjects.Config;
using ScriptableObjects.Data;
using ScriptableObjects.Variables;
using UnityEngine;
using UnityEngine.Events;
using Utils;
using SaveSystem;
using SFX;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour
{
    [Expandable] public PlayerData playerData;
    [Expandable] public PlayerConfig playerConfig;
    public BoolVariable isPlayerAlive;
    public bool isImmortal;
    public Transform hands;
    public float invulnerabilityTime = 1f;
    public UnityEvent deathEvent;
    public UnityEvent initEvent;
    public FloatEvent harmEvent;
    public FloatEvent invulnerabilityEvent;

    private float _invulnerabilityTimer;

    private void Start()
    {
        playerData.hp.value = playerConfig.maxHP.value;
        isPlayerAlive.value = true;
        if (playerData.position.value != Vector3.zero)
        {
            transform.position = playerData.position.value;
        }

        _invulnerabilityTimer = 0;
    }

    private void Update()
    {
        playerData.position.value = transform.position;
        playerData.handsPosition.value = hands.position;
        _invulnerabilityTimer += Time.deltaTime;
    }

    public void GainHP(float value)
    {
        if (playerData.hp.value != playerConfig.maxHP.value)
        {
            if (playerData.hp.value + value >= playerConfig.maxHP.value)
            {
                VoiceAudioController.Instance.Heal();
            }
        }

        playerData.hp.value = Mathf.Clamp(playerData.hp.value + value, 0, playerConfig.maxHP.value);
    }

    public void SpendHP(float value)
    {
        if (!isImmortal)
        {
            if (playerData.hp.value <= 0 || _invulnerabilityTimer < invulnerabilityTime) return;
            _invulnerabilityTimer = 0;
            if (value >= playerData.hp.value)
            {
                if (playerData.hp.value <= playerConfig.criticalHP || playerData.hp.value >= playerConfig.maxHP.value)
                {
                    Kill();
                }
                else
                {
                    var harmDamage = playerData.hp.value - playerConfig.criticalHP;
                    playerData.hp.value = playerConfig.criticalHP;
                    harmEvent.Invoke(harmDamage);
                    invulnerabilityEvent.Invoke(invulnerabilityTime);
                    VoiceAudioController.Instance.Critical();
                }
            }
            else
            {
                VoiceAudioController.Instance.Wounded();
                invulnerabilityEvent.Invoke(invulnerabilityTime);
                playerData.hp.value -= value;
                harmEvent.Invoke(value >= playerData.hp.value ? playerData.hp.value : value);
                PersistenceManager.Instance.statisticsManager.IncDamage(value);
            }
        }
    }

    public void Kill()
    {
        isPlayerAlive.value = false;
        playerData.hp.value = 0;
        StateMachine.PlayerState = StateMachine.PlayerStates.Die;
        deathEvent.Invoke();
        VoiceAudioController.Instance.Dead();
    }

    public void SpendRage(float value)
    {
        playerData.rage.value = Mathf.Clamp(playerData.rage.value - value, 0, playerConfig.maxRage.value);
    }

    public void GainRage(float value)
    {
        playerData.rage.value = Mathf.Clamp(playerData.rage.value + value, 0, playerConfig.maxRage.value);
    }

    public void Init()
    {
        transform.position = playerData.position.value;
        StateMachine.PlayerState = StateMachine.PlayerStates.Idle;
        initEvent.Invoke();
    }
}