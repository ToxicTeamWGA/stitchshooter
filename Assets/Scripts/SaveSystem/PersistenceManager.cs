﻿using System.Collections;
using System.IO;
using ScriptableObjects.Data;
using ScriptableObjects.Events;
using SFX;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;

namespace SaveSystem {
    public class PersistenceManager : Singleton<PersistenceManager> {
        public PersistentData data;
        public StatisticsManager statisticsManager;
        public SubtitlesManager subtitlesManager;
        public VoiceAudioController voiceAudioController;
        private string lastSave = "lastSave";
        private const string sessionKey = "Session";
        private const string statsAppendix = "Stats";

        private bool _isFirstPlay = true;

        public GameEvent onDataLoaded;

        public GameEvent onPlayerActivated;
        //TODO: think about game manager or injector

        public void SaveData(string key) {
            lastSave = key;
            data.currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
            SaveLoader.Save(new PersistentBean(data), key);
            SaveStatistics(key);
            Debug.Log("Data saved: " + key);
        }

        public void SaveStatistics(string key = "lastSave", bool SaveSession = false) {
            SaveLoader.Save(new StatisticsBean(data.levelData), key + statsAppendix);
            if (SaveSession)
                SaveLoader.Save(new StatisticsBean(data.sessionData), sessionKey);
        }

        private void Update() {
            //TODO: remove after tests
            if (Input.GetKeyDown(KeyCode.F6)) {
                LoadSave();
            }
        }

        public void LoadSave(string key) {
            if (!SaveLoader.SaveExists(key)) return;
            LoadSave(key, false);
            Debug.Log("Data loaded: " + key);
        }

        private void LoadSave(string key, bool inGame) {
            var persistentBean = SaveLoader.Load<PersistentBean>(key);
            SceneManager.LoadScene(persistentBean.currentSceneIndex);
            data.Init(persistentBean, inGame);
            LoadStatistics(key);
            onDataLoaded.Raise();
            Debug.Log("Data loaded: " + key);
        }

        //for 2nd level
        private void LoadSession() {
            var sessionBean = SaveLoader.Load<StatisticsBean>(sessionKey);
            data.Init(sessionBean, false);
        }

        public int GetLastLevel() {
            if (CanContinue()) {
                return SaveLoader.Load<PersistentBean>(lastSave).currentSceneIndex;
            }

            return 1;
        }

        private void LoadStatistics(string key) {
            try {
                var levelBean = SaveLoader.Load<StatisticsBean>(key + statsAppendix);
                data.Init(levelBean, true);
            }
            catch (FileNotFoundException e) {
                try {
                    var levelBean = SaveLoader.Load<StatisticsBean>(lastSave + statsAppendix);
                    data.Init(levelBean, true);
                }
                catch (FileNotFoundException e1) {
                    data.levelData.Clear();
                    SaveStatistics(key);
                }
            }
        }

        public void LoadSave() {
            LoadSave(lastSave, true);
            if (_isFirstPlay)
                SceneManager.activeSceneChanged += FirstPlay;
        }

        public bool CanContinue() {
            return SaveLoader.SaveExists(lastSave);
        }

        public void Restart() {
            SaveLoader.DeleteAllSaveFiles();
            data.Clear();
        }

        private void FirstPlay(Scene scene1, Scene scene2) {
            if (_isFirstPlay && scene2.name != "menu") {
                SceneManager.activeSceneChanged -= FirstPlay;
                _isFirstPlay = false;
                StartCoroutine(WaitForPlayerActivation());
            }
        }

        private IEnumerator WaitForPlayerActivation() {
            yield return new WaitForSeconds(1f);
            Debug.Log("Player activated event");
            onPlayerActivated.Raise();
        }

        protected override void Awake() {
            base.Awake();
            if (!destroyed) {
                data.playerData.Clear();
                data.sessionData.Clear();
                data.levelData.Clear();
                statisticsManager = new StatisticsManager(data);
                //TODO: put here tick value from settings
                subtitlesManager = new SubtitlesManager();
            }
        }
    }
}