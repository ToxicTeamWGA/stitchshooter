using System;
using System.Collections.Generic;
using ScriptableObjects.Data;
using UnityEngine;

namespace SaveSystem {
    [Serializable]
    public class PersistentBean {
        public float playerRage;
        public Vector3 playerPosition;
        public int currentSceneIndex;
        public List<string> playedCutScenes;

        public PersistentBean(PersistentData data) {
            playerRage = data.playerData.rage.value;
            playerPosition = data.playerData.position.value;
            currentSceneIndex = data.currentSceneIndex;
            playedCutScenes = new List<string>();
            foreach (var scene in data.playedCutScenes) {
                playedCutScenes.Add(scene);
            }
        }
    }
}