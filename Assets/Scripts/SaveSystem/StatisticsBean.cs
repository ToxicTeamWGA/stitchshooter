﻿using System;
using ScriptableObjects.Data;

namespace SaveSystem
{
    [Serializable]
    public class StatisticsBean
    {
        public int enemiesKilled;
        public int jumps;
        public int rolls;
        public float damageTaken;
        public int bulletsShot;

        public StatisticsBean(StatisticsData data)
        {
            enemiesKilled = data.enemiesKilled.value;
            jumps = data.jumps.value;
            rolls = data.rolls.value;
            damageTaken = data.damageTaken.value;
            bulletsShot = data.bulletsShot.value;
        }

        public void SetData(StatisticsData data) {
            data.enemiesKilled.value = enemiesKilled;
            data.jumps.value = jumps;
            data.rolls.value = rolls;
            data.damageTaken.value = damageTaken;
            data.bulletsShot.value = bulletsShot;
        }
    }
}
