﻿using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace SaveSystem {
    public static class SaveLoader {
        private static readonly string savesPath = Application.persistentDataPath + "/Saves/";

        //save object to file named <key>.dat
        public static void Save<T>(T objectToSave, string key) {
            Directory.CreateDirectory(savesPath);
            var formatter = GetBinaryFormatter();
            using (var fileStream = new FileStream(savesPath + key + ".dat", FileMode.Create)) {
                formatter.Serialize(fileStream, objectToSave);
            }

            Debug.Log(key + " saved");
        }

        //load data object by filename
        public static T Load<T>(string key) {
            Directory.CreateDirectory(savesPath);
            var formatter = GetBinaryFormatter();
            T returnValue;
            using (var fileStream = new FileStream(savesPath + key + ".dat", FileMode.Open)) {
                returnValue = (T) formatter.Deserialize(fileStream);
            }

            return returnValue;
        }

        //custom binary formatter
        private static BinaryFormatter GetBinaryFormatter() {
            var selector = new SurrogateSelector();

            //serialize non-serializable Vector3
            var vector3Surrogate = new Vector3SerializationSurrogate();
            selector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), vector3Surrogate);
            return new BinaryFormatter {SurrogateSelector = selector};
        }

        public static bool SaveExists(string key) {
            var path = savesPath + key + ".dat";
            return File.Exists(path);
        }

        public static void DeleteAllSaveFiles() {
            var directory = new DirectoryInfo(savesPath);
            if (directory.Exists) {
                directory.Delete(true);
            }

            Directory.CreateDirectory(savesPath);
            Debug.Log("All saves deleted: " + savesPath);
        }
    }
}