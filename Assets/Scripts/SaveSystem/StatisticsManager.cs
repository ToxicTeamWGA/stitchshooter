﻿using ScriptableObjects.Data;

namespace SaveSystem
{
    public class StatisticsManager //: MonoBehaviour
    {
        PersistentData allData;
        public StatisticsManager(PersistentData data) {
            allData = data;
        }

        public void FlushLevelStats()
        {
            allData.levelData.Clear();
        }

        public void FlushSessionStats()
        {
            allData.sessionData.Clear();
        }
        public void AddToSession()
        {
            allData.sessionData.enemiesKilled.value += allData.levelData.enemiesKilled.value;
            allData.sessionData.jumps.value += allData.levelData.jumps.value;
            allData.sessionData.rolls.value += allData.levelData.rolls.value;
            allData.sessionData.damageTaken.value += allData.levelData.damageTaken.value;
            allData.sessionData.bulletsShot.value += allData.levelData.bulletsShot.value;
        }

        public void IncKilled()
        {
            allData.levelData.enemiesKilled.value++;
        }
        public void IncJumps()
        {
            allData.levelData.jumps.value++;
        }
        public void IncRolls()
        {
            allData.levelData.rolls.value++;
        }
        public void IncDamage(float value)
        {
            allData.levelData.damageTaken.value += value;
        }
        public void IncBullets()
        {
            allData.levelData.bulletsShot.value++;
        }
    }
}
