﻿using ScriptableObjects.Events;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SaveSystem {
    public class CheckPoint : MonoBehaviour {
        public FloatGameEvent hpGainEvent;
        public float hpGain;
        public bool startCheckpoint = false;

        private void OnTriggerEnter2D(Collider2D other) {
            if (other.CompareTag("Player")) {
                Debug.Log("Checkpoint: " + transform.position);
                hpGainEvent.Raise(hpGain);
                //PersistenceManager.Instance.SaveData(name + "_" + System.DateTime.Now.ToString("yy_MM_dd_HH_mm"));
                //TODO: temporary save in one file
                if (startCheckpoint) {
                    PersistenceManager.Instance.SaveData("startLevel" + SceneManager.GetActiveScene().buildIndex);
                }

                PersistenceManager.Instance.SaveData("lastSave");
            }
        }
    }
}