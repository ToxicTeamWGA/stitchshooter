﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;
using SaveSystem;

namespace SFX
{
    [RequireComponent(typeof(AudioSource))]
    public class VoiceAudioController : MonoBehaviour
    {
        private static Lazy<VoiceAudioController> lazyInstance = new Lazy<VoiceAudioController>(CreateSingleton);

        public static VoiceAudioController Instance => lazyInstance.Value;
        public GameObject voicePlaying;
        [Header("Level0 phrases")] public AudioClip level0Load;
        public AudioClip gameBegin;
        public AudioClip[] fallFromAbove;
        public AudioClip[] musicBox;
        public AudioClip bossDescription;
        public AudioClip anotherWay;
        public AudioClip itMayWork;
        public AudioClip littleMore;
        public AudioClip bossDead;
        public AudioClip lightInTheEnd;
        public AudioClip filth;
        public AudioClip dessert;

        [Space(5)] [Header("Level1 phrases")] public AudioClip workshopLoad;
        public AudioClip workshop;
        public AudioClip wayOnSurface;
        public AudioClip bossRuinLift;
        public AudioClip landingAfterRuin;
        public AudioClip ratsDisturb;
        public AudioClip endCutscene;

        [Space(5)] [Header("Common random phrases")]
        public AudioClip[] overheatedPhrases;

        public AudioClip[] killingPhrases;
        public AudioClip[] woundedPhrases;
        public AudioClip[] healPhrases;
        public AudioClip[] lowHpPhrases;
        public AudioClip[] deathPhrases;
        public AudioClip[] loadPhrases;
        public AudioClip[] leaveInMenuPhrases;
        public AudioClip[] fullHpTriggerPhrases;
        public AudioClip[] fightStartPhrases;
        public AudioClip[] grandpaPhrases;

        [Space(5)] [Header("Settings")] public AudioMixer sounds;
        public float timeToLow = 1f;
        public float phraseChance = 15;
        public float voiceCoolDown = 2f;

        private AudioSource _audioPlayer;
        private float duration;
        private bool needRestore;
        private bool anotherWayPlayed;
        private float _lowTimer;
        private float _targetVolume;
        private float _startVolume;
        private int countToPhrase;
        private bool _firstPlay;

        private int _overheatedCounter = 1;
        private int _woundCounter = 1;
        private float _coolDownTimer;

        private RandomClip fallFromAboveRandom;
        private RandomClip musicBoxRandom;
        private RandomClip overheatedPhrasesRandom;
        private RandomClip killingPhrasesRandom;
        private RandomClip woundedPhrasesRandom;
        private RandomClip healPhrasesRandom;
        private RandomClip lowHpPhrasesRandom;
        private RandomClip deathPhrasesRandom;
        private RandomClip loadPhrasesRandom;
        private RandomClip leaveInMenuPhrasesRandom;
        private RandomClip fullHpTriggerPhrasesRandom;
        private RandomClip fightStartPhrasesRandom;
        private RandomClip grandpaPhrasesRandom;

        private void Update()
        {
            _coolDownTimer += Time.deltaTime;
            _lowTimer += Time.deltaTime;
            duration -= Time.deltaTime;
            if (duration < 0 && needRestore)
            {
                needRestore = false;
                RestoreSoundsVolume();
            }

            sounds.SetFloat("AllSoundsVolume", Mathf.Lerp(_startVolume, _targetVolume, _lowTimer / timeToLow));
        }

        private void Awake()
        {
            lazyInstance = new Lazy<VoiceAudioController>(CreateSingleton);
            _audioPlayer = GetComponent<AudioSource>();
            countToPhrase = Mathf.RoundToInt(100 / phraseChance) + Random.Range(-1, 1);
            _coolDownTimer = voiceCoolDown;
            fallFromAboveRandom = new RandomClip(fallFromAbove);
            musicBoxRandom = new RandomClip(musicBox);
            overheatedPhrasesRandom = new RandomClip(overheatedPhrases);
            killingPhrasesRandom = new RandomClip(killingPhrases);
            woundedPhrasesRandom = new RandomClip(woundedPhrases);
            healPhrasesRandom = new RandomClip(healPhrases);
            lowHpPhrasesRandom = new RandomClip(lowHpPhrases);
            deathPhrasesRandom = new RandomClip(deathPhrases);
            loadPhrasesRandom = new RandomClip(loadPhrases);
            leaveInMenuPhrasesRandom = new RandomClip(leaveInMenuPhrases);
            fullHpTriggerPhrasesRandom = new RandomClip(fullHpTriggerPhrases);
            fightStartPhrasesRandom = new RandomClip(fightStartPhrases);
            grandpaPhrasesRandom = new RandomClip(grandpaPhrases);
        }

        public bool PlayClip(AudioClip audioClip, bool ignoreCoolDown = false, float optionalDelay = 0f)
        {
            if (_coolDownTimer + optionalDelay >= voiceCoolDown || ignoreCoolDown)
            {
                _coolDownTimer = -audioClip.length;
                _audioPlayer.clip = audioClip;
                _audioPlayer.PlayDelayed(optionalDelay);
                LowSoundsOnVoice();
                return true;
            }

            return false;
        }

        public void Heal()
        {
            PlayRandomPhraseFromList(healPhrasesRandom);
        }

        public void Critical()
        {
            PlayRandomPhraseFromList(lowHpPhrasesRandom);
        }

        public void Wounded()
        {
            _woundCounter--;
            if (_woundCounter <= 0)
            {
                if (PlayRandomPhraseFromList(woundedPhrasesRandom))
                {
                    _woundCounter = 3;
                }
            }
        }

        public void Dead()
        {
            PlayRandomPhraseFromList(deathPhrasesRandom, true);
        }

        public void Loaded()
        {
            PlayRandomPhraseFromList(loadPhrasesRandom, true);
        }

        public void Exit()
        {
            PlayRandomPhraseFromList(leaveInMenuPhrasesRandom, true);
        }

        public void Easy()
        {
            PlayRandomPhraseFromList(fullHpTriggerPhrasesRandom);
        }

        public void Encounter()
        {
            PlayRandomPhraseFromList(fightStartPhrasesRandom);
        }

        public void Grandpa()
        {
            PlayRandomPhraseFromList(grandpaPhrasesRandom, true);
        }

        public void Overheat()
        {
            _overheatedCounter--;
            if (_overheatedCounter <= 0)
            {
                if (PlayRandomPhraseFromList(overheatedPhrasesRandom))
                {
                    _overheatedCounter = 3;
                }
            }
        }

        public void FallFromAbove(float delay)
        {
            PersistenceManager.Instance.subtitlesManager.setPreferedDelay(delay);
            PlayRandomPhraseFromList(fallFromAboveRandom, false, delay);
        }

        public void BossFirstHarm(float delay)
        {
            PersistenceManager.Instance.subtitlesManager.setPreferedDelay(delay);
            PlayClip(itMayWork, true, delay);
        }

        public void BossSecondHarm(float delay)
        {
            PersistenceManager.Instance.subtitlesManager.setPreferedDelay(delay);
            PlayClip(littleMore, true, delay);
        }

        public void MusicBox()
        {
            PlayRandomPhraseFromList(musicBoxRandom);
        }

        public bool PlayRandomPhraseFromList(RandomClip list, bool ignoreCoolDown = false, float optionalDelay = 0f)
        {
            return PlayClip(list.Next(), ignoreCoolDown, optionalDelay);
        }

        public void Level0Load()
        {
            PlayClip(level0Load, true);
        }

        public void Workshop()
        {
            PlayClip(workshop, true);
        }

        public void WorkshopLoad()
        {
            PlayClip(workshopLoad, true);
        }

        public void EndCutscene()
        {
            PlayClip(endCutscene, true, 5f);
        }

        public void WayOnSurface()
        {
            PlayClip(wayOnSurface, true);
        }

        public void BossRuinLift()
        {
            PlayClip(bossRuinLift, true);
        }

        public void Landing()
        {
            PlayClip(landingAfterRuin, true);
        }

        public void RatsDisturb(float delay)
        {
            PersistenceManager.Instance.subtitlesManager.setPreferedDelay(delay);
            PlayClip(ratsDisturb, true, delay);
        }

        public void StopVoice()
        {
            _coolDownTimer = 0;
            _audioPlayer.Stop();
            RestoreSoundsVolume();
        }

        public void BossDescription()
        {
            PlayClip(bossDescription, true, 1.2f);
        }

        public void BossDead()
        {
            PlayClip(bossDead, true, 2f);
        }

        public void GameBegin()
        {
            PlayClip(gameBegin, true, 2f);
        }

        public void AnotherWay()
        {
            if (anotherWayPlayed) return;
            anotherWayPlayed = true;
            PlayClip(anotherWay, true);
        }

        public void LightInTheEnd()
        {
            PlayClip(lightInTheEnd);
        }

        public void Dessert()
        {
            PlayClip(dessert);
        }

        public void Filth()
        {
            PlayClip(filth);
        }

        public bool IsVoicePlaying()
        {
            return duration > 0;
        }

        public void RandomPhrase()
        {
            countToPhrase--;
            if (countToPhrase <= 0)
            {
                if (PlayClip(killingPhrases[Random.Range(0, killingPhrases.Length)]))
                {
                    countToPhrase = Mathf.RoundToInt(100 / phraseChance) + Random.Range(-1, 1);
                }
            }
        }

        private void LowSoundsOnVoice()
        {
            duration = _audioPlayer.clip.length;
            needRestore = true;
            _startVolume = 0;
            _targetVolume = -10;
            _lowTimer = 0;
            if (voicePlaying != null)
            {
                voicePlaying.SetActive(true);
            }
        }

        private void RestoreSoundsVolume()
        {
            _startVolume = -10;
            _targetVolume = 0;
            _lowTimer = 0;
            if (voicePlaying != null)
            {
                voicePlaying.SetActive(false);
            }
        }

        private static VoiceAudioController CreateSingleton()
        {
            return FindObjectOfType<VoiceAudioController>();
        }

        public class RandomClip
        {
            private readonly AudioClip[] clips;
            private readonly Stack<AudioClip> random;

            public RandomClip(AudioClip[] clips)
            {
                this.clips = clips;
                random = new Stack<AudioClip>();
            }

            public AudioClip Next()
            {
                if (random.Count == 0)
                {
                    Shuffle(clips);
                }

                return random.Pop();
            }

            private void Shuffle(AudioClip[] ts)
            {
                var count = ts.Length;
                var last = count - 1;
                for (var i = 0; i < last; ++i)
                {
                    var r = Random.Range(i, count);
                    var tmp = ts[i];
                    ts[i] = ts[r];
                    ts[r] = tmp;
                }

                for (var i = 0; i < ts.Length; i++)
                {
                    random.Push(ts[i]);
                }
            }
        }
    }
}