﻿using UnityEngine;

namespace SFX {
    public class BossAudioController : MonoBehaviour {
        public AudioClip run;
        public AudioClip stun;
        public AudioClip harm;
        public AudioClip dead;
        public AudioClip attackRun;
        public AudioClip roar;
        public AudioSource bitePlayer;
        private AudioSource _audioPlayer;
        private bool isAttacking;

        private void Awake() {
            _audioPlayer = GetComponent<AudioSource>();
        }

        public void Harm() {
            bitePlayer.PlayOneShot(harm);
        }

        public void Stun() {
            bitePlayer.PlayOneShot(stun);
            _audioPlayer.Stop();
            isAttacking = false;
        }

        public void Dead() {
            bitePlayer.PlayOneShot(dead);
        }

        public void Roar() {
            bitePlayer.PlayOneShot(roar);
        }

        public void AttackRun() {
            _audioPlayer.PlayOneShot(attackRun);
            isAttacking = true;
        }

        public void Run(bool isRun) {
            if (isRun) {
                if (isAttacking) {
                    isAttacking = false;
                    _audioPlayer.PlayOneShot(run);
                }
                else {
                    _audioPlayer.UnPause();
                }
            }
            else {
                _audioPlayer.Pause();
            }
        }
    }
}