using UnityEngine;

namespace SFX {
    public class AllSoundsController : MonoBehaviour {
        private AudioSource[] _audioSources;

        private void Start() {
            _audioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        }

        public void PauseSounds() {
            for (var i = 0; i < _audioSources.Length; i++) {
                _audioSources[i].Pause();
            }
        }

        public void ResumeSounds() {
            for (var i = 0; i < _audioSources.Length; i++) {
                _audioSources[i].UnPause();
            }
        }
    }
}