using UnityEngine;

namespace SFX {
    [RequireComponent(typeof(AudioSource))]
    public class RatAudioController : MonoBehaviour {
        public AudioClip run;
        public AudioClip[] bite;
        public AudioClip[] harm;
        public AudioClip dead;
        public AudioSource bitePlayer;
        private AudioSource _audioPlayer;
        private bool isPlaying;
        private float _startPitch;

        private void Awake() {
            _audioPlayer = GetComponent<AudioSource>();
            _startPitch = _audioPlayer.pitch;
        }

        public void Bite() {
            _audioPlayer.pitch = _startPitch + Random.Range(-0.15f, 0.15f);
            bitePlayer.PlayOneShot(bite[Random.Range(0, bite.Length)]);
        }

        public void Harm() {
            _audioPlayer.pitch = _startPitch + Random.Range(-0.15f, 0.15f);
            bitePlayer.PlayOneShot(harm[Random.Range(0, harm.Length)]);
        }

        public void Dead() {
            _audioPlayer.pitch = _startPitch + Random.Range(-0.15f, 0.15f);
            bitePlayer.PlayOneShot(dead);
        }

        public void Run(bool isRun) {
            if (isRun) {
                if (isPlaying) {
                    _audioPlayer.pitch = _startPitch + Random.Range(-0.15f, 0.15f);
                    _audioPlayer.UnPause();
                }
                else {
                    isPlaying = true;
                    _audioPlayer.pitch = _startPitch + Random.Range(-0.15f, 0.15f);
                    _audioPlayer.PlayOneShot(run);
                }
            }
            else {
                _audioPlayer.Pause();
            }
        }
    }
}