using UnityEngine;
using Utils;

namespace SFX.Impact {
    [RequireComponent(typeof(Collider2D))]
    public class BulletImpact : MonoBehaviour {
        public LayerMask bulletLayers;
        public AudioSource audioPlayer;

        private void OnCollisionEnter2D(Collision2D other) {
            if (CollisionUtils.MaskCheck(other.gameObject.layer, bulletLayers)) {
                audioPlayer.Play();
            }
        }

        private void OnTriggerEnter2D(Collider2D other) {
            if (CollisionUtils.MaskCheck(other.gameObject.layer, bulletLayers)) {
                audioPlayer.Play();
            }
        }
    }
}