using UnityEngine;

namespace SFX {
    [RequireComponent(typeof(AudioSource))]
    public class PlayerAudioController : MonoBehaviour {
        public AudioClip[] steps;
        public AudioClip[] bloodSteps;
        public AudioClip[] lands;
        public AudioClip jump;
        public AudioClip roll;
        public AudioClip weaponSwitch;
        public AudioClip[] harm;

        private AudioSource _audioPlayer;
        private float _startPitch;
        private bool _play;

        private void Awake() {
            _audioPlayer = GetComponent<AudioSource>();
            _startPitch = _audioPlayer.pitch;
        }

        private void Update() {
            if (!_play) return;
            if (!_audioPlayer.isPlaying) {
                _audioPlayer.pitch = _startPitch + Random.Range(-0.15f, 0.15f);
                _audioPlayer.PlayOneShot(steps[Random.Range(0, steps.Length)]);
            }
        }

        public void Jump() {
            _play = false;
            _audioPlayer.pitch = _startPitch + Random.Range(-0.15f, 0.15f);
            _audioPlayer.PlayOneShot(jump);
        }

        public void Land() {
            _audioPlayer.pitch = _startPitch + Random.Range(-0.15f, 0.15f);
            _audioPlayer.PlayOneShot(lands[Random.Range(0, lands.Length)]);
        }

        public void Run(bool isRun) {
            _play = isRun;
        }

        public void Roll() {
            _audioPlayer.pitch = _startPitch + Random.Range(-0.15f, 0.15f);
            _audioPlayer.PlayOneShot(roll);
        }

        public void Harm() {
            _audioPlayer.pitch = _startPitch + Random.Range(-0.15f, 0.15f);
            _audioPlayer.PlayOneShot(harm[Random.Range(0, harm.Length)]);
        }

        public void WeaponSwitch() {
            _audioPlayer.pitch = _startPitch + Random.Range(-0.15f, 0.15f);
            _audioPlayer.PlayOneShot(weaponSwitch);
        }

        public void Stop() {
            _play = false;
            _audioPlayer.Stop();
        }
    }
}