﻿using UnityEngine;

namespace SFX {
    [RequireComponent(typeof(AudioSource))]
    public class MainMusicController : MonoBehaviour{
        public AudioClip mainTheme;
        public AudioClip bossFight;
        private AudioSource _audioPlayer;

        private void Awake() {
            _audioPlayer = GetComponent<AudioSource>();
            _audioPlayer.clip = mainTheme;
        }

        public void BossFight() {
            _audioPlayer.clip = bossFight;
            _audioPlayer.Play();
        }
        
        public void MainTheme() {
            _audioPlayer.clip = mainTheme;
            _audioPlayer.Play();
        }

        public void PauseMusic() {
            _audioPlayer.Pause();
        }

        public void ResumeMusic() {
            _audioPlayer.UnPause();
        }
    }
}