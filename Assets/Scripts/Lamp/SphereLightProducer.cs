using UnityEngine;
using Utils;

namespace Lamp {
    public class SphereLightProducer : MonoBehaviour {
        public SphereLight lightObject;
        public Transform container;

        public void ReleaseLights(float hp) {
            while (hp > 0) {
                var sphere = ObjectFactory.Instance.GetObject(lightObject, container);
                hp = sphere.Fill(hp);
                sphere.transform.position = transform.position;
                sphere.Throw();
            }
        }
    }
}