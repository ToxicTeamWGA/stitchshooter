using UnityEngine;

namespace Lamp {
    public class SphereStorage : MonoBehaviour {
        public SphereLightProducer producer;
        private float _hp;

        public void Store(float hp) {
            _hp += hp;
        }

        public void ReleaseAll() {
            producer.ReleaseLights(_hp);
            _hp = 0;
        }
    }
}