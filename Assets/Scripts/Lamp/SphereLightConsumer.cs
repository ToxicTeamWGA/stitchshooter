using Events;
using UnityEngine;
using Utils;

namespace Lamp {
    [RequireComponent(typeof(Collider2D))]
    public class SphereLightConsumer : MonoBehaviour {
        public float radius;
        public LayerMask lightSphereLayer;
        public int maxSpheres;
        public FloatEvent sphereCollected;

        private Collider2D[] _spheres;
        private bool isStopped;
        private float _stopTimer;

        private void Awake() {
            _spheres = new Collider2D[maxSpheres];
        }

        private void FixedUpdate() {
            if (isStopped) {
                if (_stopTimer != -1) {
                    _stopTimer -= Time.deltaTime;
                    if (_stopTimer > 0) return;
                    else {
                        isStopped = false;
                    }
                }
                else {
                    return;
                }
            }

            var size = Physics2D.OverlapCircleNonAlloc(transform.position, radius, _spheres, lightSphereLayer);
            for (var i = 0; i < size; i++) {
                //TODO: move fly behaviour from SphereLight here
                var sphereLight = _spheres[i].GetComponent<SphereLight>();
                sphereLight.Fly(this);
            }
        }

        private void OnTriggerEnter2D(Collider2D other) {
            if (CollisionUtils.MaskCheck(other.gameObject.layer, lightSphereLayer) && !isStopped) {
                var sphereLight = other.GetComponent<SphereLight>();
                sphereCollected.Invoke(sphereLight.GetHP());
                sphereLight.ReturnToPool();
            }
        }

        private void OnDrawGizmos() {
            Gizmos.DrawWireSphere(transform.position, radius);
        }

        public void Stop(float time = -1) {
            isStopped = true;
            _stopTimer = time;
        }

        public bool IsStopped() {
            return isStopped;
        }
    }
}