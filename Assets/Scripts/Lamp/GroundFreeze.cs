using UnityEngine;

namespace Lamp {
    public class GroundFreeze : MonoBehaviour {
        public LayerMask groundLayer;
        public Rigidbody2D rigidBody;
        public float freezeRadius;

        private void Update() {
            rigidBody.constraints = Physics2D.OverlapCircle(transform.position, freezeRadius, groundLayer)
                ? RigidbodyConstraints2D.FreezeAll
                : RigidbodyConstraints2D.None;
        }

        private void OnDisable() {
            rigidBody.constraints = RigidbodyConstraints2D.None;
        }

        private void OnDrawGizmos() {
            Gizmos.DrawWireSphere(transform.position, freezeRadius);
        }
    }
}