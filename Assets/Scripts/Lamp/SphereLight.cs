using UnityEngine;
using Utils;

namespace Lamp {
    [RequireComponent(typeof(Rigidbody2D), typeof(Throwable))]
    public class SphereLight : PoolObject {
        public float maxHpContains = 0.2f;
        public float lifeTime;
        public float waitTime;
        public float accelerationTime;
        public float velocity;
        public AnimationCurve velocityCurve;
        public GameObject groundTouch;
        public AudioSource audioSource;
        public LayerMask player;

        private Rigidbody2D _rb;
        private Throwable _throwable;
        private bool fly;
        private SphereLightConsumer _target;
        private float _accelerationTimer;
        private float _lifeTimer;
        private float _waitTimer;
        private float _hp;
        private Collider2D _collider;
        private float _startPitch;

        private void Awake() {
            _rb = GetComponent<Rigidbody2D>();
            _throwable = GetComponent<Throwable>();
            _collider = GetComponent<Collider2D>();
            _collider.enabled = false;
            _lifeTimer = 0;
            _waitTimer = 0;
            _startPitch = audioSource.pitch;
        }

        public override void Init() {
            base.Init();
            groundTouch.SetActive(true);
            _collider.enabled = false;
            fly = false;
            _hp = 0;
            _lifeTimer = 0;
            _accelerationTimer = 0;
            _waitTimer = 0;
            _target = null;
        }

        public float Fill(float hp) {
            if (hp > maxHpContains) {
                _hp = maxHpContains;
                return hp - maxHpContains;
            }
            else {
                _hp = hp;
                return 0;
            }
        }

        public float GetHP() {
            return _hp;
        }

        private void FixedUpdate() {
            _lifeTimer += Time.deltaTime;
            _waitTimer += Time.deltaTime;
            if (_waitTimer < waitTime) return;
            if (_lifeTimer >= lifeTime) ReturnToPool();
            _collider.enabled = true;
            if (!fly || !_target) return;
            if (_target.IsStopped()) {
                groundTouch.SetActive(true);
                fly = false;
                _rb.velocity = Vector2.zero;
                _lifeTimer = 0;
            }

            _accelerationTimer += Time.deltaTime;
            var direction = (_target.transform.position - transform.position).normalized *
                            Mathf.Lerp(0, velocity, velocityCurve.Evaluate(_accelerationTimer / accelerationTime));
            _rb.AddForce(direction);
        }

        public void Fly(SphereLightConsumer target) {
            if (fly) return;
            if (CollisionUtils.MaskCheck(target.transform.parent.gameObject.layer, player)) {
                audioSource.pitch = _startPitch + Random.Range(-0.15f, 0.15f);
                audioSource.Play();
            }

            _lifeTimer = 0;
            groundTouch.SetActive(false);
            fly = true;
            _target = target;
        }

        public void Throw() {
            _throwable.Throw(transform.parent);
        }
    }
}