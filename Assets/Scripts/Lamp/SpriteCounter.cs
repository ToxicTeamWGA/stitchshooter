using ScriptableObjects.Variables;
using UnityEngine;

namespace Lamp {
    [RequireComponent(typeof(SpriteRenderer))]
    public class SpriteCounter : MonoBehaviour {
        public Sprite[] sprites = new Sprite[10];
        public FloatVariable points;
        public float pointsInOne;

        private int _current;
        private SpriteRenderer _renderer;

        private void Awake() {
            _renderer = GetComponent<SpriteRenderer>();
        }

        private void Update() {
            var ceil = Mathf.CeilToInt(points.value / pointsInOne);
            if (_current != ceil) {
                _current = ceil;
                _renderer.sprite = sprites[ceil];
            }
        }
    }
}