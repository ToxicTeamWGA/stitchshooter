using ScriptableObjects.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace Utils {
    [RequireComponent(typeof(Image))]
    public class ImageColorSwitcher : MonoBehaviour {
        public Color trueColor;
        public Color falseColor;
        public BoolVariable switcher;

        private bool _prev;
        private Image _imageRenderer;

        private void Start() {
            _imageRenderer = GetComponent<Image>();
            _imageRenderer.color = falseColor;
            _prev = false;
        }

        private void Update() {
            if (switcher.value != _prev) {
                _prev = switcher.value;
                _imageRenderer.color = switcher.value ? trueColor : falseColor;
            }
        }
    }
}