using UnityEngine;
using UnityEngine.Events;

namespace Utils {
    [RequireComponent(typeof(Collider2D))]
    public class Trigger : MonoBehaviour {
        public LayerMask triggerLayer;
        public UnityEvent onTouch;
        public int maxTouches = 1;

        private void OnCollisionEnter2D(Collision2D other) {
            OnTouch(other.gameObject.layer);
        }

        private void OnTriggerEnter2D(Collider2D other) {
            OnTouch(other.gameObject.layer);
        }

        private void OnTouch(int layer) {
            if (((1 << layer) & triggerLayer) != 0 && maxTouches != 0) {
                maxTouches--;
                onTouch.Invoke();
            }
        }
    }
}