using UnityEngine;

namespace Utils {
    [RequireComponent(typeof(UnityEngine.Experimental.Rendering.Universal.Light2D))]
    public class LightBlink : MonoBehaviour {
        public bool loop = true;
        public bool playOnAwake;
        public float min;
        public float max;
        public float time;
        public AnimationCurve curve;

        private UnityEngine.Experimental.Rendering.Universal.Light2D _light;
        private float _timer;
        private bool playing;

        private void Awake() {
            _light = GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>();
            if (playOnAwake) {
                Play();
            }
        }

        private void Update() {
            if (!playing) return;
            _timer += Time.deltaTime;
            _light.intensity = Mathf.Lerp(min, max, curve.Evaluate(_timer / time));
            if (_timer > time) {
                if (loop) {
                    _timer = 0;
                }
                else {
                    playing = false;
                }
            }
        }

        public void Play() {
            _timer = 0;
            playing = true;
        }

        public void Play(float blinkTime) {
            _timer = 0;
            playing = true;
            time = blinkTime;
        }
    }
}