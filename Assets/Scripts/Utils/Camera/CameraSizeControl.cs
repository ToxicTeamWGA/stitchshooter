using System;
using UnityEngine;

namespace Utils.Camera {
    public class CameraSizeControl : MonoBehaviour {
        public float sizeStep = 1;
        public float maxSize = 10;
        public float minSize = 5;
        public AnimationCurve curve;
        public float timeToChange;
        private UnityEngine.Camera _sceneCamera;
        private float _current;
        private float _target;
        private float _timer;

        private void Awake() {
            _sceneCamera = GetComponent<UnityEngine.Camera>();
            _current = _target = _sceneCamera.orthographicSize;
        }

        private void Update() {
            _timer += Time.deltaTime;
            var temp = _sceneCamera.orthographicSize + Input.mouseScrollDelta.y * sizeStep;
            if (temp > minSize && temp < maxSize) {
                SetSizeInternal(temp);
            }

            if (Math.Abs(_target - _current) > 0.0001) {
                SetSizeInternal(Mathf.Lerp(_current, _target, curve.Evaluate(_timer / timeToChange)));
            }
        }

        private float GetFieldOfView(float size) {
            return Mathf.Atan2(size, -_sceneCamera.transform.position.z) * Mathf.Rad2Deg * 2;
        }

        private void SetSizeInternal(float size) {
            _sceneCamera.orthographicSize = size;
            _sceneCamera.fieldOfView = GetFieldOfView(size);
        }

        public void SetSize(float size) {
            _current = _sceneCamera.orthographicSize;
            _target = size;
            _timer = 0;
        }
    }
}