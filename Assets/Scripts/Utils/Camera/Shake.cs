using ScriptableObjects.Variables;
using UnityEngine;

namespace Utils.Camera {
    public class Shake : MonoBehaviour {
        public Vector3Variable cameraPosition;
        public Vector3 direction;
        public float strength = 1f;
        public float animationTime;
        public AnimationCurve curve;

        private float _time;
        private bool play;

        private void Update() {
            _time += Time.unscaledDeltaTime;
            if (play) {
                var position = cameraPosition.value;
                position = Vector3.Lerp(position,
                    position + direction,
                    curve.Evaluate(_time / animationTime));
                cameraPosition.value = position;
                if (_time > animationTime) {
                    play = false;
                }
            }
        }

        public void Play(Vector3 direction) {
            _time = 0;
            play = true;
            this.direction = direction.normalized * strength;
        }
    }
}