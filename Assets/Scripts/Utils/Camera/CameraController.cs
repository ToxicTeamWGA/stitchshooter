using ScriptableObjects.Variables;
using UnityEngine;

namespace Utils.Camera
{
    [RequireComponent(typeof(UnityEngine.Camera))]
    public class CameraController : MonoBehaviour
    {
        public Vector3Variable cameraPosition;
        public SpriteRenderer spriteToBoundLeft;
        public SpriteRenderer spriteToBoundRight;
        public Vector3 bottomLeft;
        public Vector3 topRight;
        public bool usePoints;
        private UnityEngine.Camera _sceneCamera;

        private void Awake()
        {
            _sceneCamera = GetComponent<UnityEngine.Camera>();
        }

        private void Start()
        {
            if (cameraPosition.value != Vector3.zero)
            {
                transform.position = cameraPosition.value;
            }
        }

        private void Update()
        {
            var boundsMin = usePoints ? bottomLeft : spriteToBoundLeft.bounds.min;
            var boundsMax = usePoints ? topRight : spriteToBoundRight.bounds.max;
            var screenAspect = (float) Screen.width / Screen.height;
            var cameraHeightHalf = _sceneCamera.orthographicSize;
            var cameraWidthHalf = cameraHeightHalf * screenAspect;

            cameraPosition.value = new Vector3(
                Mathf.Min(Mathf.Max(cameraPosition.value.x, boundsMin.x + cameraWidthHalf),
                    boundsMax.x - cameraWidthHalf),
                Mathf.Min(Mathf.Max(cameraPosition.value.y, boundsMin.y + cameraHeightHalf),
                    boundsMax.y - cameraHeightHalf),
                cameraPosition.value.z);

            transform.position = cameraPosition.value;
        }

        public void SetMin(Vector3 min)
        {
            bottomLeft = min;
        }

        public void SetMax(Vector3 max)
        {
            topRight = max;
        }

        public void SetMaxHeight(float value) {
            if(usePoints)
                topRight.y = value;
        }
    }
}