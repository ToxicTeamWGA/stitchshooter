﻿using ScriptableObjects.Variables;
using UnityEngine;

namespace Utils.Camera {
    public class FollowCamera : MonoBehaviour {
        [Header("References")] public Vector3Variable followTarget;
        public Vector3Variable cameraPosition;
        public SpriteRenderer spriteToBoundLeft;
        public SpriteRenderer spriteToBoundRight;
        public Vector3 bottomLeft;
        public Vector3 topRight;
        public bool usePoints;

        [Header("Camera Movement")] public float dampTime = 0.15f;

        public float xOffset = 0.5f;
        public float yOffset = 0.5f;
        public float xPointerKoef = 0.01f;
        public float yPointerKoef = 0.01f;
        public float maxYDelta = 10;
        public float maxXDelta = 20;

        private Vector3 _velocity = Vector3.zero;
        private UnityEngine.Camera _sceneCamera;
        private Vector3 _cameraPosition;

        private void Awake() {
            _sceneCamera = GetComponent<UnityEngine.Camera>();
        }

        private void Start() {
            cameraPosition.value =
                new Vector3(followTarget.value.x, followTarget.value.y, _sceneCamera.transform.position.z);
        }

        void Update() {
            if (followTarget) {
                var screenAspect = (float) Screen.width / Screen.height;
                var cameraHeightHalf = _sceneCamera.orthographicSize;
                var cameraWidthHalf = cameraHeightHalf * screenAspect;
                var targetPosition = followTarget.value;
                var screenPoint = Input.mousePosition;
                screenPoint.z = 10f; 
                var offset = _sceneCamera.ScreenToWorldPoint(screenPoint) - targetPosition;
                offset.x = Mathf.Clamp(offset.x, -maxXDelta, maxXDelta);
                offset.y = Mathf.Clamp(offset.y, -maxYDelta, maxYDelta);
                Vector3 delta = targetPosition -
                                _sceneCamera.ViewportToWorldPoint(new Vector3(xOffset - offset.x * xPointerKoef,
                                    yOffset - offset.y * yPointerKoef, 10));
                var position = _sceneCamera.transform.position;
                var boundsMin = usePoints ? bottomLeft : spriteToBoundLeft.bounds.min;
                var boundsMax = usePoints ? topRight : spriteToBoundRight.bounds.max;

                var newPosition = position + delta;
                newPosition = new Vector3(
                    Mathf.Min(Mathf.Max(newPosition.x, boundsMin.x + cameraWidthHalf),
                        boundsMax.x - cameraWidthHalf),
                    Mathf.Min(Mathf.Max(newPosition.y, boundsMin.y + cameraHeightHalf), boundsMax.y - cameraHeightHalf),
                    newPosition.z);
                cameraPosition.value = Vector3.SmoothDamp(position, newPosition, ref _velocity, dampTime);
            }
        }
    }
}