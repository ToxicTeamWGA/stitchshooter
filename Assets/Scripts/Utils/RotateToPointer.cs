using ScriptableObjects.Variables;
using UnityEngine;

namespace Utils {
    public class RotateToPointer : MonoBehaviour {
        public Vector3Variable anchor;
        public Vector3Variable pointerPosition;
        public Transform offset;

        [Header("Options")] public bool isFlipHorizontal;
        public bool isFlipVertical;
        public bool isRotate;

        private float _scaleX;
        private float _scaleY;
        private bool _useDefault;

        private void Awake() {
            var localScale = transform.localScale;
            _scaleY = localScale.y;
            _scaleX = localScale.x;
            _useDefault = anchor == null;
        }

        private void Update() {
            var direction = (Vector2) pointerPosition.value;
            var anchorPosition = _useDefault ? transform.position : anchor.value;

            if (isRotate) {
                var position = new Vector2(anchorPosition.x, anchorPosition.y + offset.localPosition.y);
                transform.eulerAngles =
                    new Vector3(0, 0, Vector2.SignedAngle(Vector2.right, direction - position));
            }

            var flipX = anchorPosition.x;
            var transformLocalScale = transform.localScale;
            var x = transformLocalScale.x;
            var y = transformLocalScale.y;

            if (isFlipHorizontal) {
                x = direction.x < flipX ? -_scaleX : _scaleX;
            }

            if (isFlipVertical) {
                y = direction.x < flipX ? -_scaleY : _scaleY;
            }

            transform.localScale = new Vector3(x, y, 1);
        }
    }
}