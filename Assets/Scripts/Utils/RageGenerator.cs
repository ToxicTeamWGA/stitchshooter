using Attributes;
using ScriptableObjects.Events;
using ScriptableObjects.Variables;
using UnityEngine;

namespace Utils {
    public class RageGenerator : MonoBehaviour {
        public FloatGameEvent rageEvent;
        [Expandable]
        public FloatVariable amount;

        public void Gain() {
            rageEvent.Raise(amount.value);
        }
    }
}