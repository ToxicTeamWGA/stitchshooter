﻿using System;
using System.Collections;
using Attributes;
using ScriptableObjects.Config;
using ScriptableObjects.Variables;
using SFX;
using Shooting;
using Shooting.Weapons;
using UnityEngine;
using SaveSystem;

namespace Utils {
    public class Controller2D : MonoBehaviour, IMovementConfuser {
        [Expandable] public ControllerConfig config;
        public float pushForce;
        public float pushAngle;
        [Header("References")] public PhysicsMaterial2D noFrictionMaterial;

        public Collider2D rollCollider;
        public WeaponsContainer weapons;
        public Vector3Variable pointerPosition;
        public Animator animator;
        public Transform body;
        public Transform hands;
        public PlayerAudioController audioPlayer;
        public float accelerationTime;
        public float decelerationTime;
        public AnimationCurve velocityCurve;

        private Weapon _currentWeapon;

        private int _jumps;
        private int _rolls;
        private Rigidbody2D _rb;
        private Collider2D _bodyCollider;
        private Action<StateMachine.PlayerStates> _stateHandler;
        private StateMachine.PlayerStates _currentState;
        private readonly RaycastHit2D[] _hits = new RaycastHit2D[1];
        private bool _isGrounded;
        private float movementSlow;
        private float shootConfuseTime;
        private float _accelerationTimer;
        private float _decelerationTimer;
        private Vector3 _boundsSize;
        private static readonly int crouch = Animator.StringToHash("Crouch");
        private static readonly int hSpeed = Animator.StringToHash("Horizontal Speed");
        private static readonly int vSpeed = Animator.StringToHash("Vertical Speed");
        private static readonly int rollGun = Animator.StringToHash("RollGun");
        private static readonly int rollGunBack = Animator.StringToHash("RollGunBack");
        private static readonly int rollShotGun = Animator.StringToHash("RollShotGun");
        private static readonly int rollShotGunBack = Animator.StringToHash("RollShotGunBack");
        private static readonly int jumpStart = Animator.StringToHash("JumpStart");
        private static readonly int backward = Animator.StringToHash("Backward");
        private static readonly int grounded = Animator.StringToHash("Grounded");
        private static readonly int roll = Animator.StringToHash("Roll");

        // Start is called before the first frame update
        void Start() {
            _jumps = config.maxJumps;
            _bodyCollider = body.GetComponent<Collider2D>();
            _boundsSize = _bodyCollider.bounds.size;
            Cursor.visible = false;
        }

        private void Awake() {
            _rb = GetComponent<Rigidbody2D>();
            _rb.freezeRotation = true;
            _stateHandler = state => { _currentState = state; };
            StateMachine.AddEventHandler(_stateHandler);
            _decelerationTimer = decelerationTime;
        }


        private bool IsGrounded() {
            var isGrounded = Physics2D.BoxCastNonAlloc(transform.position, _boundsSize, 0,
                                 Vector2.down, _hits, config.groundDistance, config.groundLayerMask) > 0;
            animator.SetBool(grounded, isGrounded);

            if (!_isGrounded && isGrounded) {
                audioPlayer.Land();
            }

            _isGrounded = isGrounded;
            _rb.sharedMaterial = isGrounded ? null : noFrictionMaterial;
            return isGrounded;
        }

        // Update is called once per frame
        void Update() {
            if (!_currentWeapon) {
                CreateWeapon(weapons.gun);
            }

            switch (_currentState) {
                case StateMachine.PlayerStates.Idle:
                    HandleWeaponSwitch();
                    HandleJump();
                    HandleMovement();
                    HandleShooting();
                    IsGrounded();
                    break;
                case StateMachine.PlayerStates.Roll:
                    HandleJump();
                    break;
                case StateMachine.PlayerStates.CutScene:
                    IsGrounded();
                    break;
            }

            animator.SetFloat(hSpeed, Mathf.Abs(_rb.velocity.x));
            animator.SetFloat(vSpeed, _rb.velocity.y);
        }

        public void ShootConfuse(float time, float movementSlow) {
            shootConfuseTime = time;
            this.movementSlow = movementSlow;
        }

        private void HandleShooting() {
            if (_currentWeapon) {
                if (Input.GetMouseButtonUp(0)) {
                    _currentWeapon.ShootRelease();
                }

                if (Input.GetMouseButtonDown(0)) {
                    _currentWeapon.Shoot(true);
                }

                if (Input.GetMouseButton(0)) {
                    _currentWeapon.ShootHold(true);
                }
            }
        }

        private void HandleWeaponSwitch() {
            if (Input.GetButtonDown("Gun")) {
                CreateWeapon(weapons.gun);
            }
            else if (Input.GetButtonDown("Shotgun")) {
                CreateWeapon(weapons.shotgun);
            }
        }

        public void Push() {
            if (Mathf.Abs(_rb.velocity.x) < 0.1) {
                _rb.velocity = config.movementSpeed * pushForce * Vector2.up;
            }
            else {
                _rb.velocity =
                    Quaternion.AngleAxis(IsRightFaced() ? pushAngle : -pushAngle,
                        IsBackWard(_rb.velocity.x) ? Vector3.back : Vector3.forward) *
                    _rb.velocity * pushForce;
            }
        }

        private void CreateWeapon(Weapon weapon) {
            if (!_currentWeapon || weapon.GetType() != _currentWeapon.GetType()) {
                if (_currentWeapon) {
                    _currentWeapon.ReturnToPool();
                }

                audioPlayer.WeaponSwitch();
                _currentWeapon = ObjectFactory.Instance.GetObject<Weapon>(weapon, hands);
                _currentWeapon.Init(this);
            }
        }

        private void HandleJump() {
            if (IsGrounded()) {
                _rb.gravityScale = config.gravityUp;
            }
            else if (_rb.velocity.y < 0.01) {
                _rb.gravityScale = config.gravityDown;
            }

            if (Input.GetButtonDown("Jump") && !Input.GetButton("Down")) {
                if (IsGrounded()) {
                    _jumps = config.maxJumps;
                    Jump();
                }
                else if (_jumps > 0) {
                    _rb.gravityScale = config.moreJumpGravityUp;
                    Jump();
                }
            }
        }

        private void Jump() {
            PersistenceManager.Instance.statisticsManager.IncJumps();
            animator.Play(jumpStart);
            audioPlayer.Jump();
            _jumps--;
            var xVel = _rb.velocity.x;
            _rb.velocity = new Vector2(xVel, config.jumpSpeed);
        }

        private void HandleMovement() {
            var isGrounded = IsGrounded();
            var left = Input.GetButton("Left");
            var right = Input.GetButton("Right");
            if (left && right) return;
            var horizontal = 0;
            var velocity = shootConfuseTime > 0
                ? movementSlow * config.movementSpeed
                : config.movementSpeed;
            if (left || right) {
                _accelerationTimer += Time.deltaTime;
                horizontal = right ? 1 : -1;
                animator.SetBool(backward, IsBackWard(horizontal));
                velocity = accelerationTime > 0
                    ? Mathf.Lerp(0, velocity * horizontal,
                        velocityCurve.Evaluate(_accelerationTimer / accelerationTime))
                    : velocity * horizontal;
                _rb.velocity = new Vector2(velocity, _rb.velocity.y);
                audioPlayer.Run(isGrounded);
                _decelerationTimer = 0;
            }
            else {
                horizontal = _rb.velocity.x > 0 ? 1 : -1;
                _decelerationTimer += Time.deltaTime;
                velocity = decelerationTime > 0
                    ? Mathf.Lerp(velocity * horizontal, 0,
                        velocityCurve.Evaluate(_decelerationTimer / decelerationTime))
                    : 0;
                _rb.velocity = new Vector2(velocity, _rb.velocity.y);
                _accelerationTimer = 0;
                audioPlayer.Run(false);
            }

            shootConfuseTime -= Time.deltaTime;

            animator.SetBool(crouch, Input.GetButton("Down"));
            if (_rolls < config.maxAirRolls && isGrounded) {
                _rolls = config.maxAirRolls;
            }

            if (_rolls > 0 && Input.GetButtonDown("Roll")) {
                horizontal = left ? -1 : right ? 1 : 0;
                StartCoroutine(Roll(horizontal));
            }
        }

        private bool IsBackWard(float horizontal) {
            var isRightFaced = IsRightFaced();
            return horizontal < 0 && isRightFaced || horizontal > 0 && !isRightFaced;
        }

        private float CalculateDirection(float horizontal) {
            if (horizontal > 0) {
                return 1;
            }

            if (horizontal < 0) {
                return -1;
            }

            return IsRightFaced() ? 1 : -1;
        }

        private bool IsRightFaced() {
            return transform.position.x < pointerPosition.value.x;
        }

        private int GetRollAnimation(bool isBack) {
            var isGun = _currentWeapon.GetType() == weapons.gun.GetType();
            if (isBack) {
                if (isGun) {
                    return rollGunBack;
                }
                else {
                    return rollShotGunBack;
                }
            }
            else {
                if (isGun) {
                    return rollGun;
                }
                else {
                    return rollShotGun;
                }
            }
        }

        private IEnumerator Roll(float horizontal) {
            PersistenceManager.Instance.statisticsManager.IncRolls();
            _rolls--;
            animator.Play(GetRollAnimation(IsBackWard(horizontal)));
            animator.SetBool(roll, true);
            audioPlayer.Roll();

            StateMachine.PlayerState = StateMachine.PlayerStates.Roll;
            var directionKoef = CalculateDirection(horizontal);
            _rb.velocity = new Vector2(directionKoef * config.movementSpeed * config.rollKoef, _rb.velocity.y);
            _bodyCollider.isTrigger = true;
            rollCollider.isTrigger = false;
            yield return new WaitForSeconds(config.rollDuration);
            rollCollider.isTrigger = true;
            _bodyCollider.isTrigger = false;
            StateMachine.PlayerState = StateMachine.PlayerStates.Idle;
            _rb.velocity = new Vector2(directionKoef * config.movementSpeed * config.rollFallback, _rb.velocity.y);
            animator.SetBool(roll, false);
        }

        private void OnCollisionStay2D(Collision2D other) {
            if (other.gameObject.CompareTag("Platform")) {
                if (Input.GetButton("Down") && Input.GetButton("Jump")) {
                    _bodyCollider.isTrigger = true;
                }
            }
        }

        private void OnTriggerExit2D(Collider2D other) {
            if (other.gameObject.CompareTag("Platform")) {
                _bodyCollider.isTrigger = false;
            }
        }

        public void EnableControl() {
            StateMachine.PlayerState = StateMachine.PlayerStates.Idle;
        }

        public void DisableControl() {
            StateMachine.PlayerState = StateMachine.PlayerStates.CutScene;
            audioPlayer.Stop();
        }
    }
}