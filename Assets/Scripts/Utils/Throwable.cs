using UnityEngine;
using Random = UnityEngine.Random;

namespace Utils {
    [RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
    public class Throwable : MonoBehaviour {
        [Header("Throw Params")] public float dropSpeed;
        public float torque;

        public int angleRadius;
        private Rigidbody2D _rb;

        private void Awake() {
            _rb = GetComponent<Rigidbody2D>();
        }

        public void Throw(Transform parent) {
            Vector2 randomDirectionWithUnitLength =
                Quaternion.Euler(0f, 0f, Random.Range(-angleRadius / 2, angleRadius / 2)) * Vector2.up;
            transform.parent = parent;
            _rb.velocity = randomDirectionWithUnitLength * dropSpeed;
            _rb.AddTorque(torque);
        }

        public void Throw() {
            Throw(null);
        }
    }
}