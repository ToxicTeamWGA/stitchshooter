using UnityEngine;

namespace Utils {
    public class FollowParent : MonoBehaviour {
        public Transform parent;
        public float _xOffset;
        public float _yOffset;

        private void Update() {
            transform.position = new Vector3(parent.position.x + _xOffset, parent.position.y + _yOffset,
                transform.position.z);
        }
    }
}