using UnityEngine;

namespace Utils {
    public class PositionToPinter : MonoBehaviour {
        [Header("References")] public Transform pointer;

        private void Update() {
            transform.position = pointer.position;
        }
    }
}