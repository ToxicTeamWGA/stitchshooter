using UnityEngine;

namespace Utils
{
    public class TrainController : MonoBehaviour
    {
        public float speed;
        public Collider2D[] colliders;

        private void FixedUpdate()
        {
            transform.position = Vector2.MoveTowards(transform.position, Vector2.right * 10000, speed);
        }

        public void ActivateColliders()
        {
            for (var i = 0; i < colliders.Length; i++)
            {
                colliders[i].enabled = true;
            }
        }
    }
}