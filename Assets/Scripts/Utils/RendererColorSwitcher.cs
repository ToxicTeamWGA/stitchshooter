using ScriptableObjects.Variables;
using UnityEngine;

namespace Utils {
    [RequireComponent(typeof(SpriteRenderer))]
    public class RendererColorSwitcher : MonoBehaviour {
        public Color trueColor;
        public Color falseColor;
        public BoolVariable switcher;

        private bool _prev;
        private SpriteRenderer _spriteRenderer;

        private void Start() {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _spriteRenderer.color = falseColor;
            _prev = false;
        }

        private void LateUpdate() {
            if (switcher.value != _prev) {
                _prev = switcher.value;
                _spriteRenderer.color = switcher.value ? trueColor : falseColor;
            }
        }
    }
}