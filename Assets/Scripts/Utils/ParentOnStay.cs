using UnityEngine;

namespace Utils
{
    public class ParentOnStay : MonoBehaviour
    {
        private void OnCollisionEnter2D(Collision2D other)
        {
            other.gameObject.transform.SetParent(transform);
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            other.gameObject.transform.SetParent(null);
        }
    }
}