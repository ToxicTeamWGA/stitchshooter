using System;
using Shooting.Weapons;

namespace Utils {
    [Serializable]
    public struct WeaponsContainer {
        public Weapon gun;
        public Weapon shotgun;
    }
}