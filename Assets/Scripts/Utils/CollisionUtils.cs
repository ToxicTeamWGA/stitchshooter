using UnityEngine;

namespace Utils {
    public static class CollisionUtils {
        public static bool MaskCheck(int layer, LayerMask mask) {
            return ((1 << layer) & mask) != 0;
        }
    }
}