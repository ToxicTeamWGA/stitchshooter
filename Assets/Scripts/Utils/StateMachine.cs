﻿using System;

namespace Utils {
    public class StateMachine : Singleton<StateMachine> {
        public static PlayerStates PlayerState {
            set => Instance.OnStateChanged?.Invoke(value);
        }

        private event Action<PlayerStates> OnStateChanged;

        public static void AddEventHandler(Action<PlayerStates> handler) {
            Instance.OnStateChanged += handler;
        }

        public static void RemoveEventHandler(Action<PlayerStates> handler) {
            Instance.OnStateChanged -= handler;
        }

        public enum PlayerStates {
            Idle,
            Roll,
            CutScene,
            Die
        }
    }
}