using UnityEngine;

namespace Utils {
    public class Parallax : MonoBehaviour {
        public GameObject sceneCamera;
        public float parallaxEffect;
        public int changeDirectionParts = 2;

        private float part, startPosition;

        private void Awake() {
            startPosition = transform.position.x;
            part = GetComponent<SpriteRenderer>().bounds.size.x / changeDirectionParts;
        }

        private void Update() {
            var cameraX = sceneCamera.transform.position.x;
            float dist;
            if (((int) (cameraX / part) % 2 != 0)) {
                dist = (part - cameraX % part) * parallaxEffect;
            }
            else {
                dist = cameraX % part * parallaxEffect;
            }

            var transformPosition = transform.position;
            transformPosition.x = startPosition + dist;
            transform.position = transformPosition;
        }
    }
}