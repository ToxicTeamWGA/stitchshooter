using Health;
using UnityEngine;

namespace Environment {
    [RequireComponent(typeof(Collider2D))]
    public class SurfaceWithDamage : MonoBehaviour, IDamager {
        public float damage;
        public bool playerKill;
        public float Damage => damage;

        private void OnCollisionEnter2D(Collision2D other) {
            if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
                if (playerKill) {
                    other.gameObject.GetComponent<Player>().Kill();
                }
                else {
                    other.gameObject.GetComponent<Player>().SpendHP(damage);
                }
            }
        }
    }
}