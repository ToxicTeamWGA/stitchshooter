using UnityEngine;
using Utils;

namespace Environment {
    [RequireComponent(typeof(Collider2D))]
    public class WallRunController : MonoBehaviour {
        public float speed;
        public LayerMask playerLayer;

        private void Update() {
            transform.position =
                Vector2.MoveTowards(transform.position, transform.position + Vector3.right * speed,
                    speed * Time.deltaTime);
        }

//        private void OnTriggerEnter2D(Collider2D other) {
//            if (CollisionUtils.MaskCheck(other.gameObject.layer, playerLayer)) {
//                other.GetComponent<Player>().Kill();
//            }
//        }

        private void OnCollisionEnter2D(Collision2D other) {
            if (CollisionUtils.MaskCheck(other.gameObject.layer, playerLayer)) {
                other.gameObject.GetComponent<Player>().Kill();
            }
        }
    }
}