using UnityEngine;
using UnityEngine.Playables;

namespace Environment {
    public class PumpController : MonoBehaviour {
        public PlayableDirector playable;
        public Collider2D pushCollider;

        private void OnTriggerEnter2D(Collider2D other) {
            if (!(other.attachedRigidbody.velocity.y < 0) || playable.state == PlayState.Playing) return;
            playable.Play();
            pushCollider.enabled = false;
        }
    }
}