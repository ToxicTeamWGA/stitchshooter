﻿using Health;
using UnityEngine;

namespace Environment {
    [RequireComponent(typeof(ParticleSystem))]
    [RequireComponent(typeof(Collider2D))]
    public class ParticleDamager : MonoBehaviour, IDamager {
        public float Damage => damage;
        public float damage;

        private Collider2D _particleCollider;
        private ParticleSystem _particleSystem;

        public void Start() {
            _particleCollider = GetComponent<Collider2D>();
            _particleCollider.enabled = false;
            _particleSystem = GetComponent<ParticleSystem>();
        }

        public void Update() {
            if (_particleSystem.isPlaying && !_particleCollider.enabled) _particleCollider.enabled = true;
            if (_particleSystem.isStopped && _particleCollider.enabled) _particleCollider.enabled = false;
        }
    }
}