using SFX;
using UnityEngine;

namespace Environment
{
    public class ElevatorController : MonoBehaviour
    {
        public DistanceJoint2D leftRope;
        public DistanceJoint2D rightRope;
        public AudioSource audioPlayer;
        public AudioClip run;
        public AudioClip destroy;
        public AudioClip slide;
        public AudioClip stop;
        public Rigidbody2D rb;
        public GameObject enemiesContainer;
        public GameObject cable;
        private bool isDestroying;
        private bool isDestroyed;
        private float disableTimer;

        public void Destroy()
        {
            VoiceAudioController.Instance.BossRuinLift();
            isDestroying = true;
        }

        public void Run()
        {
            audioPlayer.PlayOneShot(run);
        }

        public void Slide()
        {
            audioPlayer.PlayOneShot(slide);
        }

        public void Stop()
        {
            audioPlayer.Stop();
            audioPlayer.PlayOneShot(stop);
        }

        private void Update()
        {
            if (isDestroying && !VoiceAudioController.Instance.IsVoicePlaying())
            {
                isDestroyed = true;
                //TODO: Rope animation;
                rb.sharedMaterial = null;
                leftRope.enabled = false;
                rightRope.enabled = false;
                cable.SetActive(false);
                isDestroying = false;
                audioPlayer.PlayOneShot(destroy);
            }

            if (isDestroyed)
            {
                disableTimer += Time.deltaTime;
                if (disableTimer >= 2f)
                {
                    enemiesContainer.SetActive(false);
                }
            }
        }
    }
}