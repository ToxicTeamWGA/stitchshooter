using Shooting.Weapons.Bullets;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Utils;

namespace Environment {
    [RequireComponent(typeof(Collider2D))]
    public class SteamController : MonoBehaviour {
        [Header("Valve")] public Transform valve;
        public float valveEndAngle;
        public float animationValveTime;
        public LayerMask collisionLayers;
        public AnimationCurve valveCurve;

        [Header("Steam")] public float openTime;
        public Transform cap;
        public AnimationCurve capCurve;
        public float cupAnimationTime;
        public ParticleSystem particles;
        public AudioSource audioPlayer;
        public AudioSource incorrect;

        public UnityEvent onOpen;
        public UnityEvent onClose;

        public Image[] vibes;

        private float _startArrowAngle;
        private float _targetArrowAngle;
        private float _arrowTimeToDown;
        private float _arrowTimeToUp;
        private float _valveTimeToDown;
        private float _steamOpenTime;
        private float _capAnimationTime;
        private bool _isOpen;
        private bool _canOpen;

        private void Awake() {
            _capAnimationTime = cupAnimationTime;
        }

        private void Update() {
            _valveTimeToDown += Time.deltaTime;
            _steamOpenTime += Time.deltaTime;
            _capAnimationTime += Time.deltaTime;

            if (_isOpen) {
                var colorA = Mathf.Lerp(1, 0, _steamOpenTime / openTime);
                for (var i = 0; i < vibes.Length; i++) {
                    Color color = vibes[i].color;
                    color.a = colorA;
                    vibes[i].color = color;
                }

                if (_steamOpenTime >= openTime) {
                    Close();
                }
            }

            if (_steamOpenTime >= openTime && _isOpen) {
                Close();
            }

            valve.transform.eulerAngles = new Vector3(0, 0,
                Mathf.Lerp(valveEndAngle, 0, valveCurve.Evaluate(_valveTimeToDown / animationValveTime)));
            cap.eulerAngles = new Vector3(0, 0, Mathf.Lerp(_isOpen ? 0 : 90, _isOpen ? 90 : 0,
                capCurve.Evaluate(_capAnimationTime / cupAnimationTime)));
        }

        private void Open() {
            if (!_isOpen) {
                particles.Play();
                audioPlayer.Play();
                onOpen.Invoke();
                _capAnimationTime = 0;
                _steamOpenTime = 0;
                _isOpen = true;
                _canOpen = false;
            }
        }

        private void Close() {
            if (_isOpen) {
                particles.Stop();
                audioPlayer.Stop();
                onClose.Invoke();
                _capAnimationTime = 0;
                _isOpen = false;
            }
        }

        private void OnTriggerEnter2D(Collider2D other) {
            if (!CollisionUtils.MaskCheck(other.gameObject.layer, collisionLayers) || !other.attachedRigidbody) return;
            _valveTimeToDown = 0;
            valve.transform.eulerAngles = new Vector3(0, 0, valveEndAngle);
            var bullet = other.GetComponent<Bullet>();
            if (bullet) {
                bullet.ReturnToPool();
            }

            if (!_isOpen && _canOpen) {
                Open();
            }
            else {
                incorrect.Play();
            }
        }

        public void Activate() {
            _canOpen = true;
        }
    }
}