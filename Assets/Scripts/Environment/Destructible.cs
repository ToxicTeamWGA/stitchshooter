﻿using Health;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace Environment {
    public class Destructible : MonoBehaviour {
        public LayerMask layerMask;
        public GameObject destroyEffect;
        public GameObject destroyedObject;
        public float health = 80f;
        public float explosionForce = 100f;
        public UnityEvent onDestroy;
        private GameObject parts;
        private Rigidbody2D[] _partsRb;

        private void OnCollisionEnter2D(Collision2D other) {
            if (CollisionUtils.MaskCheck(other.gameObject.layer, layerMask)) {
                health -= other.gameObject.GetComponent<IDamager>().Damage;
                Debug.Log("Object damage: " + other.gameObject.GetComponent<IDamager>().Damage);
                if (health <= 0) {
                    parts.SetActive(true);
                    Instantiate(destroyEffect, transform.position, Quaternion.identity);
                    foreach (var part in _partsRb) {
                        part.AddForce((part.gameObject.transform.position - transform.position) * explosionForce);
                    }

                    onDestroy.Invoke();
                }
            }
        }

        private void Start() {
            parts = Instantiate(destroyedObject, transform.position, transform.rotation);
            parts.SetActive(false);
            _partsRb = parts.GetComponentsInChildren<Rigidbody2D>();
        }
    }
}