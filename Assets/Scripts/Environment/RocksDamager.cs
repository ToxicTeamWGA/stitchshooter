using Health;
using UnityEngine;

namespace Environment {
    public class RocksDamager : MonoBehaviour, IDamager {
        public float damage;

        public float Damage => damage;
    }
}