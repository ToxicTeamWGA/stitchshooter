using ScriptableObjects.Variables;
using UnityEngine;

namespace Environment
{
    public class WheelController : MonoBehaviour
    {
        public Vector3Variable position;
        public AudioSource audioPlayer;
        public AudioClip[] clips;

        private void Update()
        {
            position.value = transform.position;
        }

        public void Harm()
        {
            audioPlayer.PlayOneShot(clips[Random.Range(0, clips.Length)]);
        }
    }
}