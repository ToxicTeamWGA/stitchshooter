using ScriptableObjects.Config;
using ScriptableObjects.Variables;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

namespace HUD {
    public class HPColor : MonoBehaviour {
        [Header("Color")] public FloatVariable playerHP;
        public PlayerConfig config;
        public Color startColor;
        public Color endColor;
        public Light2D[] lights;
        public SpriteRenderer shadow;

        [Header("Critical blink")] public float time;
        public AnimationCurve curve;

        private float _timer;
        private float[] lightsIntensity;

        private void Awake() {
            lightsIntensity = new float[lights.Length];
            for (var i = 0; i < lights.Length; i++) {
                lightsIntensity[i] = lights[i].intensity;
            }

            _timer = 0;
        }

        private void FixedUpdate() {
            var newColor = Color.Lerp(endColor, startColor, (playerHP.value - 1) / (config.maxHP.value - 1));
            for (var i = 0; i < lights.Length; i++) {
                lights[i].color = newColor;
                if (playerHP.value <= config.criticalHP) {
                    _timer += Time.deltaTime;
                    lights[i].intensity = Mathf.Lerp(0, lightsIntensity[i], curve.Evaluate(_timer / time));
                }
                else {
                    lights[i].intensity = lightsIntensity[i];
                }
            }

            shadow.color = newColor;

            if (_timer > time) {
                _timer = 0;
            }
        }
    }
}