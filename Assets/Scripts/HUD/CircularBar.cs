using System;
using Attributes;
using ScriptableObjects.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace HUD {
    [RequireComponent(typeof(Image))]
    public class CircularBar : MonoBehaviour {
        [Expandable] public FloatVariable current;
        [Expandable] public FloatVariable max;
        [Expandable] public FloatVariable min;
        public float maxFill = 0.25f;
        public float animationTime;
        public float minAlpha = 0.25f;
        public AnimationCurve curve;
        public float deltaToAnimate = 0.001f;
        public bool changeAlpha = true;

        private Image _bar;
        private float _time;
        private Color color;

        private void Awake() {
            _bar = GetComponent<Image>();
            if (min == null) {
                min = ScriptableObject.CreateInstance<FloatVariable>();
            }

            color = _bar.color;
            _time = animationTime;
        }

        public void Update() {
            _time += Time.deltaTime;
            var newValue = maxFill * (current.value - min.value) / (max.value - min.value);
            if (Math.Abs(_bar.fillAmount - newValue) < deltaToAnimate) {
                _time = 0;
            }

            _bar.fillAmount = Mathf.Lerp(_bar.fillAmount,
                newValue, curve.Evaluate(_time / animationTime));
            if (changeAlpha) {
                color.a = Math.Max(minAlpha, _bar.fillAmount / maxFill);
                _bar.color = color;
            }
        }
    }
}