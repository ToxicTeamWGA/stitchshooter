﻿using UnityEngine;

namespace HUD {
    public class HintTrigger : MonoBehaviour {
        public SpriteRenderer hintSprite;
        private bool _isUsed;

        private Animator _hintAnimator;
        private readonly int fadeIn = Animator.StringToHash("FadeIn");

        public void OnTriggerEnter2D(Collider2D other) {
            if (!other.CompareTag("Player") || _isUsed) return;
            hintSprite.gameObject.SetActive(true);
            _isUsed = true;
        }

        public void Start() {
            _isUsed = false;
            hintSprite.gameObject.SetActive(false);
        }
    }
}