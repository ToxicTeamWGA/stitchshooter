﻿using Attributes;
using ScriptableObjects.Variables;
using UnityEngine;

namespace ScriptableObjects.Data
{
    [CreateAssetMenu(menuName = "Data/Statistics Data")]
    public class StatisticsData : ScriptableObject
    {
        [Expandable] public IntVariable enemiesKilled;
        [Expandable] public IntVariable jumps;
        [Expandable] public IntVariable rolls;
        [Expandable] public FloatVariable damageTaken;
        [Expandable] public IntVariable bulletsShot;

        public void Clear()
        {
            enemiesKilled.value = 0;
            jumps.value = 0;
            rolls.value = 0;
            damageTaken.value = 0;
            bulletsShot.value = 0;
        }
    }
}
