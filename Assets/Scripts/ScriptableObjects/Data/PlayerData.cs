using Attributes;
using ScriptableObjects.Variables;
using UnityEngine;

namespace ScriptableObjects.Data
{
    [CreateAssetMenu(menuName = "Data/Player Data")]
    public class PlayerData : ScriptableObject
    {
        [Expandable] public FloatVariable hp;
        [Expandable] public FloatVariable rage;
        [Expandable] public BoolVariable isEnraged;
        [Expandable] public Vector3Variable position;
        [Expandable] public Vector3Variable handsPosition;

        public void Clear()
        {
            hp.value = 0;
            rage.value = 0;
            isEnraged.value = false;
            position.value = Vector3.zero;
            handsPosition.value = Vector3.zero;
        }
    }
}