﻿using System.Collections.Generic;
using Attributes;
using SaveSystem;
using UnityEngine;

namespace ScriptableObjects.Data {
    [CreateAssetMenu(menuName = "Data/Persistent Data")]
    public class PersistentData : ScriptableObject {
        [Expandable] public PlayerData playerData;
        [Expandable] public StatisticsData sessionData;
        [Expandable] public StatisticsData levelData;
        public HashSet<string> playedCutScenes = new HashSet<string>();
        public int currentSceneIndex;

        public void Init(PersistentBean persistentBean, bool ignoreCutScenes) {
            playerData.rage.value = persistentBean.playerRage;
            playerData.position.value = persistentBean.playerPosition;
            currentSceneIndex = persistentBean.currentSceneIndex;
            if (!ignoreCutScenes) {
                playedCutScenes.Clear();
                foreach (var scene in persistentBean.playedCutScenes) {
                    playedCutScenes.Add(scene);
                }
            }
        }

        public void Init(StatisticsBean statisticsBean, bool levelStats) {
            if (levelStats)
                statisticsBean.SetData(levelData);
            else statisticsBean.SetData(sessionData);
        }

        public void Clear() {
            playedCutScenes.Clear();
            playerData.Clear();
            sessionData.Clear();
            levelData.Clear();
            playedCutScenes.Clear();
        }
    }
}