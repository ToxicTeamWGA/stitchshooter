using UnityEngine;

namespace ScriptableObjects.Variables {
    [CreateAssetMenu(menuName = "Variables/Float")]
    public class FloatVariable : ScriptableObject {
        public float value;
    }
}