using UnityEngine;

namespace ScriptableObjects.Variables
{
    [CreateAssetMenu(menuName = "Variables/Vector3")]
    public class Vector3Variable : ScriptableObject
    {
        public Vector3 value;

        public void Clear()
        {
            value = Vector3.zero;
        }
    }
}