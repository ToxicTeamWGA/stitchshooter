using UnityEngine;

namespace ScriptableObjects.Variables {
    [CreateAssetMenu(menuName = "Variables/Boolean")]
    public class BoolVariable : ScriptableObject {
        public bool value;
    }
}