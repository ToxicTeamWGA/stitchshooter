using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjects.Events {
    public class GameEventWithParam<T, E, U> : ScriptableObject
        where E : GameEventWithParam<T, E, U> where U : UnityEvent<T> {
        private readonly List<GameEventListenerWithParam<T, E, U>> _listeners =
            new List<GameEventListenerWithParam<T, E, U>>();

        public void Raise(T value) {
            for (var i = _listeners.Count - 1; i >= 0; i--) {
                _listeners[i].OnEventRaised(value);
            }
        }

        public void RegisterListener(GameEventListenerWithParam<T, E, U> listener) {
            _listeners.Add(listener);
        }

        public void UnRegisterListener(GameEventListenerWithParam<T, E, U> listener) {
            _listeners.Remove(listener);
        }
    }
}