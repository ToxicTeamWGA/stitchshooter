using Events;
using UnityEngine;

namespace ScriptableObjects.Events {
    [CreateAssetMenu(menuName = "Events/Float Game Event")]
    public class FloatGameEvent : GameEventWithParam<float, FloatGameEvent, FloatEvent> {
    }
}