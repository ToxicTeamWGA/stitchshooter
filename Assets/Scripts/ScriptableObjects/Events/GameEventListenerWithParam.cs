using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjects.Events {
    public class GameEventListenerWithParam<T, E, U> : MonoBehaviour
        where E : GameEventWithParam<T, E, U> where U : UnityEvent<T> {
        public E Event;
        public U response;

        private void OnEnable() {
            Event.RegisterListener(this);
        }

        private void OnDisable() {
            Event.UnRegisterListener(this);
        }

        public void OnEventRaised(T value) {
            response.Invoke(value);
        }
    }
}