using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjects.Events {
    public class GameEventListener : MonoBehaviour {
        public GameEvent Event;
        public UnityEvent response;

        private void OnEnable() {
            Event.RegisterListener(this);
        }

        private void OnDisable() {
            Event.UnRegisterListener(this);
        }

        public void OnEventRaised() {
            response.Invoke();
        }
    }
}