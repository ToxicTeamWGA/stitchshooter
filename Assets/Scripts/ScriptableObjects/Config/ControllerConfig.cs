using UnityEngine;

namespace ScriptableObjects.Config {
    [CreateAssetMenu(menuName = "Config/Controller2D")]
    public class ControllerConfig : ScriptableObject {
        [Header("Movement")] public float movementSpeed;
        public float deadZone;
        [Header("Roll")] public float rollKoef;
        public float rollDuration;
        public float rollFallback;
        [Header("Jump")] public float jumpSpeed;
        public float groundDistance;
        public int maxJumps = 2;
        public int maxAirRolls = 1;
        public float gravityUp = 3f;
        public float moreJumpGravityUp = 4f;
        public float gravityDown = 4.5f;
        
        public LayerMask groundLayerMask;
    }
}