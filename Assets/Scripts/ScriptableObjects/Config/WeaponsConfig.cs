using Attributes;
using UnityEngine;

namespace ScriptableObjects.Config {
    [CreateAssetMenu(menuName = "Config/Weapons")]
    public class WeaponsConfig : ScriptableObject {
        [Expandable]
        public SlingshotConfig slingshot;
    }
}