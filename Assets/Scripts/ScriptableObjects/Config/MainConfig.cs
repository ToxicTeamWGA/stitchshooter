using Attributes;
using ScriptableObjects.Config.Enemy;
using UnityEngine;

namespace ScriptableObjects.Config {
    [CreateAssetMenu(menuName = "Config/Main")]
    public class MainConfig : ScriptableObject {
        [Expandable]
        public WeaponsConfig weapons;
        [Expandable]
        public EnemyConfig[] enemies;
    }
}