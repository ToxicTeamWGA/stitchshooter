using UnityEngine;

namespace ScriptableObjects.Config.Enemy {
    [CreateAssetMenu(menuName = "Config/Enemies/RatBoss")]
    public class RatBossConfig : BossConfig {
        public float damage;
    }
}