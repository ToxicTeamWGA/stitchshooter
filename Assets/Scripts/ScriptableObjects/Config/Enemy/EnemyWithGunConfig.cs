using UnityEngine;

namespace ScriptableObjects.Config.Enemy {
    [CreateAssetMenu(menuName = "Config/Enemies/EnemyWithGun")]
    public class EnemyWithGunConfig : EnemyConfig {
        public float attackRandomMin;
        public float attackRandomMax;
    }
}