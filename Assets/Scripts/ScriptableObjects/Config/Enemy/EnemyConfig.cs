using System;
using Attributes;
using ScriptableObjects.Variables;
using UnityEngine;

namespace ScriptableObjects.Config.Enemy {
    [Serializable]
    public class EnemyConfig : ScriptableObject {
        public float attackRange;
        public float baseSpeed;
        public float groundDistance;
        public LayerMask groundLayerMask;
        public float attackCoolDown = 1f;
    }
}