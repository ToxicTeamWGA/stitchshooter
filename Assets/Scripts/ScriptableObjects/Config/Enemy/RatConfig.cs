using UnityEngine;

namespace ScriptableObjects.Config.Enemy {
    [CreateAssetMenu(menuName = "Config/Enemies/Rat")]
    public class RatConfig : EnemyConfig {
        public float speedRandomBounds; //max value to add or subtract from speed 
        public float leapForce;
        public float nextWaypointDistMax;
        public float prepareTime = 1f;
    }
}