using Attributes;
using ScriptableObjects.Variables;
using UnityEngine;

namespace ScriptableObjects.Config {
    [CreateAssetMenu(menuName = "Config/Slingshot Config")]
    public class SlingshotConfig : ScriptableObject {
        [Expandable]
        public FloatVariable maxSpeed;
        [Expandable]
        public FloatVariable minSpeed;
        public float speedIncreasePerSecond;
    }
}