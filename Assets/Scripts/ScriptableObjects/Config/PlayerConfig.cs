using Attributes;
using ScriptableObjects.Variables;
using UnityEngine;

namespace ScriptableObjects.Config {
    [CreateAssetMenu(menuName = "Config/Player")]
    public class PlayerConfig : ScriptableObject {
        [Expandable] public FloatVariable maxHP;
        [Expandable] public FloatVariable maxRage;
        [Expandable] public ControllerConfig controller;
        public float criticalHP = 1f;
    }
}