﻿using UnityEngine;
using UnityEngine.Audio;

namespace UI {
    public class VolumeSettings : MonoBehaviour {
        public AudioMixer audioMixer;

        public void SetOverallVolume(float volume) {
            audioMixer.SetFloat("OverallVolume", volume);
        }

        public void SetMusicVolume(float volume) {
            audioMixer.SetFloat("MusicVolume", volume);
        }

        public void SetEffectsVolume(float volume) {
            audioMixer.SetFloat("EffectsVolume", volume);
            audioMixer.SetFloat("VoiceVolume", volume);
        }
    }
}