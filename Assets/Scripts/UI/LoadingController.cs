using System;
using System.Collections;
using SFX;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI {
    public class LoadingController : MonoBehaviour {
        public GameObject visible;
        public Slider loadingSlider;
        public RawImage loadingLight;

        public GameObject pressAnyKeyText;
        public GameObject sliderPanel;

        public void Load(int sceneIndex, Action<AsyncOperation> onLoaded = null) {
            StartCoroutine(LoadScene(sceneIndex, onLoaded));
        }

        public void Next() {
            Load(SceneManager.GetActiveScene().buildIndex + 1);
        }

        private IEnumerator LoadScene(int sceneIndex, Action<AsyncOperation> onLoaded) {
            visible.SetActive(true);
            //TODO: remove and make by action
            switch (sceneIndex) {
                case 1:
                    VoiceAudioController.Instance.Level0Load();
                    break;
                case 2:
                    VoiceAudioController.Instance.WorkshopLoad();
                    break;
            }

            yield return new WaitForSeconds(0.01f);

            var color = loadingLight.color;
            color.a = 0.1f;
            loadingLight.color = color;

            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneIndex, LoadSceneMode.Single);
            asyncLoad.allowSceneActivation = false;
            if (onLoaded != null) {
                asyncLoad.completed += onLoaded;
            }

            while (asyncLoad.progress < .9f) {
                loadingSlider.value = asyncLoad.progress;
                color.a = asyncLoad.progress;
                loadingLight.color = color;
                yield return null;
            }

            pressAnyKeyText.SetActive(true);
            sliderPanel.SetActive(false);
            while (!Input.anyKey) {
                yield return null;
            }

            asyncLoad.allowSceneActivation = true;
        }
    }
}