﻿using SaveSystem;
using SFX;
using UnityEngine;

namespace UI {
    public class MainMenu : MonoBehaviour {
        public GameObject menu;
        public GameObject continueButton;
        public MainMusicController mainMusicController;
        public LoadingController loadingController;
        public LoadingController loadingControllerVar;

        private void Start() {
            Cursor.visible = true;
            continueButton.SetActive(PersistenceManager.Instance.CanContinue());
            mainMusicController.MainTheme();
        }

        public void NewGame() {
            PersistenceManager.Instance.Restart();
            menu.SetActive(false);
            loadingController.Load(1);
        }

        public void Continue() {
            menu.SetActive(false);
            switch (PersistenceManager.Instance.GetLastLevel()) {
                case 1:
                    loadingController.Load(PersistenceManager.Instance.GetLastLevel(),
                        operation => PersistenceManager.Instance.LoadSave());
                    break;
                case 2:
                    loadingControllerVar.Load(PersistenceManager.Instance.GetLastLevel(),
                        operation => PersistenceManager.Instance.LoadSave());
                    break;
            }
            
        }

        public void Quit() {
            Application.Quit();
        }

        public void ToggleSubtitles(bool isChecked) {
            PersistenceManager.Instance.subtitlesManager.turnOnSubtitles = isChecked;
        }
    }
}