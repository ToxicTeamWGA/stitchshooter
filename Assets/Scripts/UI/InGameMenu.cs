﻿using SaveSystem;
using SFX;
using Timelines;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

namespace UI {
    public class InGameMenu : MonoBehaviour {
        public GameObject menu;
        public GameObject optionsMenu;
        public GameObject confirmMenu;
        public Controller2D player;
        public CutSceneManager cutSceneManager;
        public Image panel;
        public AllSoundsController soundsController;
        public GameObject endMenu;

        private void Update() {
            if (Input.GetKeyDown(KeyCode.Escape) && !cutSceneManager.IsCutScenePlaying() &&
                !endMenu.activeInHierarchy) {
                if (menu.activeInHierarchy) {
                    Close();
                    soundsController.ResumeSounds();
                    //_audioSource.Stop();
                }
                else if (optionsMenu.activeInHierarchy) {
                    optionsMenu.SetActive(false);
                    menu.SetActive(true);
                }
                else if (confirmMenu.activeInHierarchy) {
                    confirmMenu.SetActive(false);
                    menu.SetActive(true);
                }
                else {
                    Open();
                    soundsController.PauseSounds();
                    //_audioSource.Play();
                }
            }
        }

        private void Open() {
            menu.SetActive(true);
            SetPanelAlpha(.9f);
            Time.timeScale = 0;
            player.DisableControl();
            Cursor.visible = true;
        }

        private void SetPanelAlpha(float a) {
            var panelColor = panel.color;
            panelColor.a = a;
            panel.color = panelColor;
        }

        public void Close() {
            menu.SetActive(false);
            SetPanelAlpha(0);
            Time.timeScale = 1;
            player.EnableControl();
            Cursor.visible = false;
        }

        public void RestartLevel() {
            Close();
            PersistenceManager.Instance.LoadSave("startLevel" + SceneManager.GetActiveScene().buildIndex);
            PersistenceManager.Instance.statisticsManager.FlushLevelStats();
        }

        public void BackToMainMenu() {
            Close();
            SceneManager.LoadScene("menu");
        }

        public void ToggleSubtitles(bool isChecked) {
            PersistenceManager.Instance.subtitlesManager.turnOnSubtitles = isChecked;
        }
    }
}