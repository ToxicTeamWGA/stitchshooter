﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

namespace UI {
    public class EndScreen : MonoBehaviour {
        public Image panel;
        public Controller2D player;
        public Button next;

        public void Show() {
            gameObject.SetActive(true);
            SetPanelAlpha(.9f);
            Time.timeScale = 0;
            player.DisableControl();
            Cursor.visible = true;
            next.onClick.Invoke();
        }
        
        public void Close() {
            gameObject.SetActive(false);
            SetPanelAlpha(0f);
            Time.timeScale = 1;
            Cursor.visible = false;
        }

        private void SetPanelAlpha(float a) {
            var panelColor = panel.color;
            panelColor.a = a;
            panel.color = panelColor;
        }

        public void MainMenu() {
            Time.timeScale = 1;
            player.EnableControl();
            SceneManager.LoadScene("menu");
        }

        public void GoToItch() {
            Application.OpenURL("https://gevernus.itch.io/theplay");
        }
    }
}