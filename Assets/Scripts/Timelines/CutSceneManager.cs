using SaveSystem;
using SFX;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

namespace Timelines {
    [RequireComponent(typeof(PlayableDirector))]
    public class CutSceneManager : MonoBehaviour {
        public CutScene startTimeline;
        public UnityEvent beforePlay;
        public UnityEvent afterPlay;
        [SerializeField] private VoiceAudioController voice;
        [SerializeField] private PlaySubtitle playSubtitle;

        private CutScene _current;
        private double _duration;
        private bool afterPlayCalled;

        private void Awake() {
            SceneManager.activeSceneChanged += OnSceneActivated;
        }

        private void OnSceneActivated(Scene scene1, Scene scene2) {
            // Debug.Log("Active scene changed Event: " + scene1.name + ", " + scene2.name);
            if (scene2.name == "level0") {
                SceneManager.activeSceneChanged -= OnSceneActivated;
                PlayStartCutScene();
            }
        }

        public void PlayStartCutScene() {
            if (startTimeline) {
                Play(startTimeline);
                startTimeline = null;
            }
        }

        private void Update() {
            _duration -= Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Escape)) {
                Skip();
            }

            if (_duration <= 0 && _current) {
                AfterPLay();
            }
        }

        private void AfterPLay() {
            if (afterPlayCalled) return;
            afterPlayCalled = true;
            afterPlay?.Invoke();
            _current.afterPlay?.Invoke();
        }

        public void Play(CutScene scene) {
            if (PersistenceManager.Instance.data.playedCutScenes.Contains(scene.name)) {
                //TODO: hack for trigger after scene event
                scene.afterPlay?.Invoke();
                return;
            }

            afterPlayCalled = false;
            _current = scene;
            beforePlay?.Invoke();
            _current.beforePlay?.Invoke();
            _current.Play();
            _duration = _current.director.duration;
            if (_current.playOneShoot) {
                PersistenceManager.Instance.data.playedCutScenes.Add(_current.name);
            }
        }

        public void Skip() {
            if (_current) {
                _current.director.time = _current.director.duration;
                voice.StopVoice();
                playSubtitle.StopSubtitles();
            }

            _duration = 0;
        }

        public bool IsCutScenePlaying() {
            return _duration > 0;
        }
    }
}