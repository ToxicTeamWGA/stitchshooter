using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;

namespace Timelines {
    [RequireComponent(typeof(PlayableDirector))]
    public class CutScene : MonoBehaviour {
        public PlayableDirector director;
        public bool playOneShoot;
        public UnityEvent beforePlay;
        public UnityEvent afterPlay;

        private void Awake() {
            director = GetComponent<PlayableDirector>();
        }

        public void Play() {
            director.Play();
        }
    }
}