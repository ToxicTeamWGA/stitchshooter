using UnityEngine;

namespace Shooting.ShootLine {
    public class ShootArcRenderer : ShootLineRenderer {
        [Header("Render")] public int resolution;
        public float groundDistance;
        public float velocity;

        private float _g;
        private float _radianAngle;

        protected override void Awake() {
            base.Awake();
            _g = Mathf.Abs(Physics2D.gravity.y);
        }

        protected override void CalculateLine(Ray2D rayToPointer) {
            var weaponNewPoint = rayToPointer.GetPoint(0);
            Vector3[] lineArray = new Vector3[resolution];
            _radianAngle = Mathf.Deg2Rad * Vector2.SignedAngle(Vector2.right,
                              rayToPointer.GetPoint(0) - rayToPointer.GetPoint(0));
            float maxDistance = (velocity * Mathf.Cos(_radianAngle) / _g) *
                                (velocity * Mathf.Sin(_radianAngle) +
                                 Mathf.Sqrt(velocity * Mathf.Sin(_radianAngle) * velocity * Mathf.Sin(_radianAngle) +
                                            2 * _g * groundDistance));
            for (int i = 0; i < resolution; i++) {
                float t = (float) i / resolution;
                lineArray[i] = CalculateLinePoint(t, maxDistance) + weaponNewPoint;
            }

            RenderLine(lineArray);
        }

        private Vector2 CalculateLinePoint(float t, float maxDistance) {
            float x = t * maxDistance;
            float y = x * Mathf.Tan(_radianAngle) -
                      ((_g * x * x) / (2 * velocity * velocity * Mathf.Cos(_radianAngle) * Mathf.Cos(_radianAngle)));
            return new Vector2(x, y);
        }
    }
}