using System;
using UnityEngine;
using Utils;

namespace Shooting.ShootLine {
    [RequireComponent(typeof(LineRenderer))]
    public class ShootLineRenderer : MonoBehaviour {
        [Header("Shoot Line")] public float shootLineDistance;
        public Transform startPoint;
        public Transform directionPoint;
        public float shootLineStartWidth;
        public float shootLineEndWidth;

        private LineRenderer _shootLine;
        private bool _isActive = true;
        private Action<StateMachine.PlayerStates> _stateHandler;

        protected virtual void Awake() {
            _shootLine = GetComponent<LineRenderer>();
            _shootLine.startWidth = shootLineStartWidth;
            _shootLine.endWidth = shootLineEndWidth;
            _stateHandler = state => {
                switch (state) {
                    case StateMachine.PlayerStates.Idle:
                        _shootLine.enabled = true;
                        ClearLine();
                        _isActive = true;
                        break;
                    case StateMachine.PlayerStates.Roll:
                    case StateMachine.PlayerStates.CutScene:
                    case StateMachine.PlayerStates.Die:
                        _shootLine.enabled = false;
                        _isActive = false;
                        break;
                }
            };
        }

        private void Update() {
            if (!_isActive) return;
            var holderPosition = startPoint.position;
            CalculateLine(new Ray2D(holderPosition, directionPoint.position - holderPosition));
        }


        protected virtual void CalculateLine(Ray2D rayToPointer) {
            RenderLine(new Vector3[] {rayToPointer.GetPoint(0), rayToPointer.GetPoint(shootLineDistance)});
        }

        protected void RenderLine(Vector3[] vertices) {
            if (vertices.Length < 2) {
                throw new ArgumentException("Vertices length must be 2 or more");
            }

            _shootLine.positionCount = vertices.Length;
            for (var i = 0; i < vertices.Length; i++) {
                _shootLine.SetPosition(i, vertices[i]);
            }
        }

        private void ClearLine() {
            var shootLinePositionCount = _shootLine.positionCount;
            _shootLine.positionCount = 0;
            _shootLine.positionCount = shootLinePositionCount;
        }

        private void OnEnable() {
            _shootLine.enabled = true;
            ClearLine();
            StateMachine.AddEventHandler(_stateHandler);
        }

        private void OnDisable() {
            _shootLine.enabled = false;
            StateMachine.RemoveEventHandler(_stateHandler);
        }
    }
}