namespace Shooting {
    public interface IMovementConfuser {
        void ShootConfuse(float time, float movementSlow);
    }
}