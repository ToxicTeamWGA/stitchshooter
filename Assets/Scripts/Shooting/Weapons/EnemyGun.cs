using Shooting.Weapons.Bullets;
using Utils;

namespace Shooting.Weapons {
    public class EnemyGun : Weapon {
        protected override void LaunchBullet() {
            var bulletObject = ObjectFactory.Instance.GetObject<Bullet>(bullet);
            var bulletDirection = GetBulletDirection();
            bulletObject.Shoot(shootPoint.position, bulletDirection, bulletSpeed);
        }
    }
}