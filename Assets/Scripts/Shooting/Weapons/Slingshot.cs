using ScriptableObjects.Config;
using ScriptableObjects.Variables;
using Shooting.ShootLine;
using Shooting.Weapons.Bullets;
using UnityEngine;
using Utils;

namespace Shooting.Weapons {
    public class Slingshot : Weapon {
        public SlingshotConfig config;

        public FloatVariable velocity;
        private ShootArcRenderer _shootArcRenderer;
        private bool _hold;

        protected override void Awake() {
            base.Awake();
            velocity.value = config.minSpeed.value;
            _shootArcRenderer = GetComponent<ShootArcRenderer>();
            _shootArcRenderer.enabled = false;
        }

        public override void Shoot(bool isCounted = false) {
        }

        protected override void LaunchBullet() {
            
        }

        public override void ShootHold(bool isCounted = false) {
            if (lastShotTime > shootCoolDown) {
                _hold = true;
                var temp = velocity.value + config.speedIncreasePerSecond * Time.unscaledDeltaTime;
                if (temp < config.maxSpeed.value) {
                    velocity.value = temp;
                }

                _shootArcRenderer.enabled = true;
                _shootArcRenderer.velocity = velocity.value;
            }
        }

        public override void ShootRelease() {
            if (_hold) {
                var bulletObject = ObjectFactory.Instance.GetObject<Bullet>(bullet);
                bulletObject.Shoot(transform.position, GetBulletDirection(), velocity.value);
                velocity.value = config.minSpeed.value;
                _shootArcRenderer.enabled = false;
                _shootArcRenderer.velocity = velocity.value;
                lastShotTime = 0f;
                _hold = false;
            }
        }
    }
}