﻿using Health;
using UnityEngine;
using Utils;

namespace Shooting.Weapons.Bullets {
    public abstract class Bullet : PoolObject, IDamager {
        [Header("Shooting")] [SerializeField] protected float damage;
        [SerializeField] protected float timeToDestroy = 3f;
        public LayerMask destroyLayer;
        private Rigidbody2D _rb;
        private float destroyTimer;

        public float Damage => damage;

        private void Awake() {
            _rb = GetComponent<Rigidbody2D>();
            Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Bullet"), LayerMask.NameToLayer("Platform"));
        }

        private void Update() {
            destroyTimer += Time.deltaTime;
            if (destroyTimer > timeToDestroy) {
                ReturnToPool();
            }
        }

        public void Shoot(Vector2 startPosition, Vector2 direction, float speed) {
            transform.position = startPosition;
            transform.right = direction;
            _rb.freezeRotation = true;
            _rb.velocity = speed * direction.normalized;
        }

        public override void Init() {
            base.Init();
            destroyTimer = 0;
        }

        private void OnCollisionEnter2D(Collision2D other) {
            if (CollisionUtils.MaskCheck(other.gameObject.layer, destroyLayer)) {
                ReturnToPool();
            }
        }

        private void OnTriggerEnter2D(Collider2D other) {
            if (CollisionUtils.MaskCheck(other.gameObject.layer, destroyLayer)) {
                ReturnToPool();
            }
        }
    }
}