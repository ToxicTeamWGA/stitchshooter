﻿using System.Collections;
using UnityEngine;
using Utils;

namespace Shooting.Weapons.Bullets {
    [RequireComponent(typeof(Rigidbody2D), typeof(AudioSource))]
    public class BulletCase : PoolObject {
        public float timeToDestroy = 5f;
        public float torqueRange = 5f;
        public AudioClip[] sounds;
        private Rigidbody2D _rb;
        private AudioSource _audioSource;
        private bool isPlayed;

        private void Awake() {
            _rb = GetComponent<Rigidbody2D>();
            _audioSource = GetComponent<AudioSource>();
        }

        public void Throw(Vector2 startPosition, Vector2 direction, float speed) {
            transform.position = startPosition;
            _rb.velocity = speed * direction.normalized;
            _rb.AddTorque(torqueRange);
            StartCoroutine(DestroyAfterSeconds(timeToDestroy));
        }

        IEnumerator DestroyAfterSeconds(float seconds) {
            yield return new WaitForSeconds(seconds);
            ReturnToPool();
        }

        public virtual void PlaySound() {
            if (!isPlayed) {
                isPlayed = true;
                _audioSource.PlayOneShot(sounds[Random.Range(0, sounds.Length)]);
            }
        }
    }
}