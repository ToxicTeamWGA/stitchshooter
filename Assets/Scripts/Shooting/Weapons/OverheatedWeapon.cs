using System;
using ScriptableObjects.Variables;
using SFX;
using UnityEngine;
using Utils;
using Random = UnityEngine.Random;

namespace Shooting.Weapons {
    public abstract class OverheatedWeapon : Weapon {
        public override void Init() {
            base.Init();
            isOverHeated.value = false;
        }

        [Header("Overheating")] public float heatPerSecond;
        public float recoveryPerSecond;
        public float recoveryWaitTime = 0.2f;
        public float overheatTime = 2f;
        public float recoveryTick = 0.4f;
        public FloatVariable heating;
        public BoolVariable isOverHeated;
        public AudioClip overheatedAudio;

        private float _heating;
        private float _heatingRecoveryLastTime;
        private float _recoveryTimer;
        private Action<StateMachine.PlayerStates> _stateHandler;


        protected override void Awake()
        {
            base.Awake();
            _stateHandler = state => {
                switch (state) {
                    case StateMachine.PlayerStates.Idle:
                        visibleParts.SetActive(true);
                        break;
                    case StateMachine.PlayerStates.Roll:
                    case StateMachine.PlayerStates.Die:
                        visibleParts.SetActive(false);
                        break;
                }
            };
        }

        protected override void OnEnable() {
            base.OnEnable();
            StateMachine.AddEventHandler(_stateHandler);
            if (heating == null) return;
            isOverHeated.value = heating.value == 100;
            var timeToRecover = Time.time - _heatingRecoveryLastTime;
            heating.value = Mathf.Clamp(_heating - timeToRecover * recoveryPerSecond, 0, 100);
            _recoveryTimer = 0;
        }

        protected override void OnDisable() {
            base.OnDisable();
            StateMachine.RemoveEventHandler(_stateHandler);
            if (heating == null) return;
            _heatingRecoveryLastTime = Time.time;
            _heating = heating.value;
        }

        protected override void Update() {
            base.Update();
            if (isOverHeated.value && lastShotTime >= overheatTime) {
                isOverHeated.value = false;
            }

            if (lastShotTime >= shootCoolDown) {
                _recoveryTimer += Time.unscaledDeltaTime;
                if (_recoveryTimer >= recoveryTick) {
                    _recoveryTimer = 0;
                    heating.value = Mathf.Clamp(heating.value - recoveryTick * recoveryPerSecond, 0, 100);
                }
            }
        }

        public override void Shoot(bool isCounted = false) {
            if (lastShotTime >= shootCoolDown) {
                if (isOverHeated.value && (!audioPlayer.isPlaying || audioPlayer.clip != overheatedAudio)) {
                    //TODO: run overheat audio and animation
                    VoiceAudioController.Instance.Overheat();
                    audioPlayer.clip = overheatedAudio;
                    audioPlayer.pitch = startPitch + Random.Range(-0.15f, 0.15f);
                    audioPlayer.Play();
                }

                if (isOverHeated.value) return;
                _recoveryTimer = recoveryTick - recoveryWaitTime;
                heating.value = Mathf.Clamp(heating.value + heatPerSecond * shootCoolDown, 0, 100);
                if (heating.value >= 100) {
                    isOverHeated.value = true;
                }
            }

            base.Shoot(isCounted);
        }
    }
}