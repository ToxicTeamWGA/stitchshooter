﻿using Events;
using Shooting.Weapons.Bullets;
using UnityEngine;
using Utils;
using Random = UnityEngine.Random;
using SaveSystem;

namespace Shooting.Weapons
{
    public abstract class Weapon : PoolObject
    {
        [Header("Events")] public Vector3Event onShoot;

        [Header("References")] [SerializeField]
        protected Bullet bullet;

        public GameObject visibleParts;
        public Transform shootPoint;
        public Transform rayPoint;
        public SpriteRenderer muzzleFlash;

        [Header("Shooting")] public float bulletSpeed = 2f;
        public float muzzleFlashTime;
        public float shootCoolDown = 0.5f;
        public float movementSlow = 1;
        public float movementSlowTime = 1;
        public AudioClip shootAudio;

        [Header("Animation")] public Animator leftHandAnimator;
        public Animator rightHandAnimator;

        [Header("BulletCase")] public BulletCase bulletCase;
        public float bulletCaseSpeed = 2f;
        public Transform bulletCasePoint;
        public float xThrowRange = 0.5f;

        protected float lastShotTime;
        protected AudioSource audioPlayer;
        protected float startPitch;
        
        private float _muzzleFLashTimer;
        private IMovementConfuser player;
        private static readonly int shoot = Animator.StringToHash("Shoot");
        
        protected virtual void Awake()
        {
            var current = transform;
            current.localPosition = Vector3.zero;
            lastShotTime = shootCoolDown;
            audioPlayer = GetComponent<AudioSource>();
            startPitch = audioPlayer.pitch;
        }

        private void Start()
        {
            transform.localPosition = Vector3.zero;
        }

        protected virtual void OnEnable()
        {
        }

        protected virtual void OnDisable()
        {
        }

        private void ThrowBulletCase()
        {
            var bulletCaseObject = ObjectFactory.Instance.GetObject<BulletCase>(bulletCase);
            var randomDirection = new Vector2(Random.Range(-xThrowRange, 0) * transform.right.x, 0.5f);
            bulletCaseObject.Throw(bulletCasePoint.position, randomDirection, bulletCaseSpeed);
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            lastShotTime += Time.unscaledDeltaTime;
            _muzzleFLashTimer += Time.unscaledDeltaTime;

            if (_muzzleFLashTimer >= muzzleFlashTime)
            {
                muzzleFlash.enabled = false;
            }
        }

        protected Vector2 GetBulletDirection()
        {
            return shootPoint.position - rayPoint.position;
        }

        protected abstract void LaunchBullet();

        public virtual void Shoot(bool isCounted = false)
        {
            if (lastShotTime >= shootCoolDown)
            {
                if (isCounted) PersistenceManager.Instance.statisticsManager.IncBullets();
                var bulletDirection = GetBulletDirection();
                player.ShootConfuse(movementSlowTime, movementSlow);
                onShoot.Invoke(-bulletDirection);
                _muzzleFLashTimer = 0;
                muzzleFlash.enabled = true;
                audioPlayer.pitch = startPitch + Random.Range(-0.05f, 0.05f);
                audioPlayer.clip = shootAudio;
                audioPlayer.Play();
                leftHandAnimator.SetTrigger(shoot);
                rightHandAnimator.SetTrigger(shoot);
                lastShotTime = 0;

                LaunchBullet();
                ThrowBulletCase();
            }
        }

        public virtual void ShootHold(bool isCounted = false)
        {
            Shoot(isCounted);
        }

        public virtual void ShootRelease()
        {
        }

        public virtual void Init(IMovementConfuser playerObject)
        {
            player = playerObject;
        }

        public override void Init()
        {
            base.Init();
            transform.localPosition = Vector3.zero;
        }
    }
}