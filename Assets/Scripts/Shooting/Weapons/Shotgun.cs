using Shooting.Weapons.Bullets;
using UnityEngine;
using Utils;

namespace Shooting.Weapons {
    public class Shotgun : OverheatedWeapon {
        [Header("Shotgun parameters")]
        public float angle;
        public float bulletCount;

        protected override void LaunchBullet() {
            var bulletDirection = GetBulletDirection();
            var anglePerBullet = angle / bulletCount;
            var angleAxis = Quaternion.AngleAxis(anglePerBullet * (bulletCount / 2), Vector3.forward);
            for (var i = 0; i < bulletCount; i++) {
                var bulletObject = ObjectFactory.Instance.GetObject<Bullet>(bullet);
                bulletObject.Shoot(shootPoint.position, angleAxis * bulletDirection, bulletSpeed);
                angleAxis *= Quaternion.AngleAxis(anglePerBullet, Vector3.back);
            }
        }
    }
}