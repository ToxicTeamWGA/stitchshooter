using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Spawn {
    public class SpawnPoint : MonoBehaviour {
        public float spawnCoolDown;
        public Transform objectContainer;
        public PoolObject[] enemies;
        private float _time;
        private readonly Stack<PoolObject> _enemies = new Stack<PoolObject>();

        private void Awake() {
            _time = spawnCoolDown;
        }

        private void Update() {
            _time += Time.deltaTime;
            if (_time <= spawnCoolDown || _enemies.Count <= 0) return;
            _time = 0;
            var enemy = ObjectFactory.Instance.GetObject<PoolObject>(_enemies.Pop(), objectContainer);
            enemy.transform.position = transform.position;
        }

        public void Spawn(params PoolObject[] enemies) {
            foreach (var enemy in enemies) {
                _enemies.Push(enemy);
            }
        }
        
        public void Spawn() {
            foreach (var enemy in enemies) {
                _enemies.Push(enemy);
            }
        }
    }
}