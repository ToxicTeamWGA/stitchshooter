using UnityEngine;
using Utils;

namespace Spawn {
    public class SpawnTrigger : MonoBehaviour {
        public SpawnPoint spawnPoint;
        public PoolObject[] enemies;

        private bool _isActive = true;

        private void OnTriggerEnter2D(Collider2D other) {
            if (!_isActive || other.gameObject.layer != LayerMask.NameToLayer("Player")) return;
            spawnPoint.Spawn(enemies);
            _isActive = false;
        }
    }
}