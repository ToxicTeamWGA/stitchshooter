using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Text {
    public class TextWordAppear : MonoBehaviour {
        public string text;
        public TextItem wordPrefab;
        public float timeToWord;
        public float whiteSpaceSize;
        public Vector3 leftPosition;
        public Vector3 rightPosition;

        private readonly List<TextItem> _words = new List<TextItem>();
        private float _time;
        private bool _isDone;
        private bool _calculated;

        private void OnEnable() {
            _time = 0;
            var textItems = text.Split(" ".ToCharArray());
            for (var i = 0; i < textItems.Length; i++) {
                var textItem = ObjectFactory.Instance.GetObject<TextItem>(wordPrefab, transform);
                SetAlpha(textItem, 0);
                textItem.Text = textItems[i];
                _words.Add(textItem);
            }

            _isDone = _words.Count == 0;
        }

        private void OnDisable() {
            _calculated = false;
            foreach (var item in _words) {
                item.ReturnToPool(transform);
            }

            _words.Clear();
        }


        private void SetAlpha(TextItem item, float value) {
            var itemColor = item.Color;
            itemColor.a = value;
            item.Color = itemColor;
        }

        private void Update() {
            if (_isDone) return;
            _time += Time.deltaTime;
            CalculatePositions();
            for (var i = 0; i < _words.Count; i++) {
                SetAlpha(_words[i], Mathf.Lerp(0, 1, (_time - i * timeToWord / _words.Count) / timeToWord));
                _isDone = (_time - i * timeToWord / _words.Count) > timeToWord;
            }
        }

        private void CalculatePositions() {
            if (_calculated) return;
            var startPosition = leftPosition;
            _calculated = true;
            for (var i = 0; i < _words.Count; i++) {
                var textItem = _words[i];
                if (Mathf.Abs(textItem.Bounds.center.x) < 0.05) {
                    _calculated = false;
                    break;
                }

                startPosition.x += textItem.Bounds.extents.x;
                var endPosition = startPosition.x + textItem.Bounds.extents.x + whiteSpaceSize;
                if (endPosition > rightPosition.x) {
                    startPosition.x = leftPosition.x + textItem.Bounds.extents.x;
                    startPosition.y -= textItem.Bounds.size.y;
                    textItem.transform.localPosition = startPosition;
                    startPosition.x += textItem.Bounds.extents.x + whiteSpaceSize;
                }
                else {
                    textItem.transform.localPosition = startPosition;
                    startPosition.x = endPosition;
                }
            }
        }

        public void SetText(string text) {
            this.text = text;
            _words.Clear();
            OnEnable();
        }
    }
}