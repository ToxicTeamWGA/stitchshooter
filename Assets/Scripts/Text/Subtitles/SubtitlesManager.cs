﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SubtitlesManager
{
    private Dictionary<string, SubData> lines = new Dictionary<string, SubData>(StringComparer.OrdinalIgnoreCase);

    public string resFile = "texts/lines";
    public bool turnOnSubtitles = true;
    private float preferedDelay = -1;
    
    public SubData GetValue(string textKey) {
        SubData tmp = null;
        if (lines.TryGetValue(textKey, out tmp))
            return tmp;

        throw new System.Exception("Failed to get subtiles (key: " + textKey + ")");
               
    }

    public void setPreferedDelay(float value) {
        preferedDelay = value;
    }

    public float getPreferedDelay() {
        return preferedDelay;
    }
    
    public void clearPreferedDelay()
    {
        setPreferedDelay(-1);
    }

    public string[] GetText(string textKey) {
        return GetValue(textKey).Lines;
    }

    public float[] GetDurations(string textKey) {
        return GetValue(textKey).Durations;
    }

    public float GetDelay(string textKey)
    {
        return GetValue(textKey).Delay;
    }

    void TurnOnSubtiles(bool value) {
        turnOnSubtitles = value;
    }

    public SubtitlesManager()
    {
        var textAsset = Resources.Load<TextAsset>(resFile);
        var voText = JsonUtility.FromJson<VoiceOverText>(textAsset.text);

        foreach (var t in voText.lines)
        {
            lines[t.key] = new SubData(t.line, t.duration, t.delay);
        }
    }
}
