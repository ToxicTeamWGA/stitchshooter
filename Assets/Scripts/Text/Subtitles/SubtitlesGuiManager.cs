﻿using UnityEngine;
using TMPro;

public class SubtitlesGuiManager : MonoBehaviour
{
    public TMP_Text textBox;
    
    public void SetText(string text) {
        textBox.text = text;
    }

    public void Clear()
    {
        textBox.text = string.Empty;
    }
}
