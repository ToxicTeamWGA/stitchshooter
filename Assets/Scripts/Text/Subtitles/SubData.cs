
public class SubData {
    public string[] Lines { get; private set; }
    public float[] Durations { get; private set; }
    public float Delay { get; private set; }

    public SubData(string[] lines, float[] durations, float delay) {
        Lines = lines;
        Durations = durations;
        Delay = delay;
    }
}