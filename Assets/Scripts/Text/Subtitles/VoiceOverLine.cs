﻿using System;

[Serializable]
public class VoiceOverLine
{
    public string key;
    public float delay;
    public string[] line;
    public float[] duration;
}
