﻿using System.Collections;
using UnityEngine;
using SaveSystem;

public struct SubOffset
{
    public int Line;
    public float Time;

    public SubOffset(int line, float time)
    {
        Line = line;
        Time = time;
    }
}


[RequireComponent(typeof(AudioSource))]
public class PlaySubtitle : MonoBehaviour
{
    private AudioSource audioSource;
    private string curAudioSourceName;
    public SubtitlesGuiManager guiManager;
    private bool audioChanged;
    private IEnumerator playSubCoroutine;


    private void Awake()
    {
        guiManager = FindObjectOfType<SubtitlesGuiManager>();
        audioSource = GetComponent<AudioSource>();
        curAudioSourceName = "";
    }

    private void Update()
    {
        CheckSrc();
        guiManager.textBox.enabled = PersistenceManager.Instance.subtitlesManager.turnOnSubtitles && audioSource.isPlaying;
        if (audioChanged && PersistenceManager.Instance.subtitlesManager.turnOnSubtitles && audioSource.isPlaying) {
            var offset = CalcOffset();
            curAudioSourceName = audioSource.clip.name;
            
            if (playSubCoroutine != null)
                StopCoroutine(playSubCoroutine);
            playSubCoroutine = DoSubtitle(offset.Line, offset.Time);
            StartCoroutine(playSubCoroutine);
            audioChanged = false;
        }
    }

    private IEnumerator DoSubtitle(int initLine = 0, float dur = 0f)
    {
        var info = PersistenceManager.Instance.subtitlesManager.GetValue(curAudioSourceName);
        var subtitle = info.Lines;
        var lineDurations = info.Durations;
        float delay;
        if (PersistenceManager.Instance.subtitlesManager.getPreferedDelay() < 0)
            delay = info.Delay;
        else
        {
            delay = PersistenceManager.Instance.subtitlesManager.getPreferedDelay();
            PersistenceManager.Instance.subtitlesManager.clearPreferedDelay();
        }
        float curTime = audioSource.timeSamples / audioSource.clip.frequency;
        Debug.Log(audioSource.time + "; " + delay + "; " + audioSource.timeSamples);
        if (delay > 0 && curTime <= 0) {
            guiManager.Clear();
            yield return new WaitForSeconds(Mathf.Abs(delay));
        }

        guiManager.SetText(subtitle[initLine]);
        yield return new WaitForSeconds(Mathf.Abs(dur));
        

        for (int i = initLine + 1; i < subtitle.Length; i++) {
            guiManager.SetText(subtitle[i]);
            if(lineDurations[i]>0)
                yield return new WaitForSeconds(lineDurations[i]);
            else yield return new WaitForSeconds(audioSource.clip.length);
        }

        guiManager.Clear();
    }

    private void CheckSrc() {
        if(audioSource.clip != null)
            audioChanged = !curAudioSourceName.Equals(audioSource.clip.name);
    }

    private SubOffset CalcOffset() {
        float curTime = audioSource.timeSamples / audioSource.clip.frequency;
        int lineNum = 0;
        var lineDurations = PersistenceManager.Instance.subtitlesManager.GetDurations(audioSource.clip.name);
        for (int i = 0; i < lineDurations.Length; i++) {
            curTime -= lineDurations[i];
            if (curTime <= 0) {
                lineNum = i;
                break;
            }
        }
        return new SubOffset(lineNum, curTime);
    }

    public void StopSubtitles() {
        if (playSubCoroutine != null)
        {
            StopCoroutine(playSubCoroutine);
            playSubCoroutine = null;
        }
        guiManager.Clear();
    }
}
