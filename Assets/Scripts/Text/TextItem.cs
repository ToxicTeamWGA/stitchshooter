using TMPro;
using UnityEngine;
using Utils;

namespace Text {
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TextItem : PoolObject {
        public Color Color {
            get => textMesh.color;
            set => textMesh.color = value;
        }

        public string Text {
            get => textMesh.text;
            set => textMesh.text = value;
        }

        public Bounds Bounds => textMesh.textBounds;

        private TextMeshProUGUI textMesh;

        private void Awake() {
            textMesh = GetComponent<TextMeshProUGUI>();
        }

        public override void ReturnToPool(Transform parent = null) {
            Destroy(gameObject);
        }

        public override void Init() {
            transform.localScale = new Vector3(1, 1, 1);
            textMesh.autoSizeTextContainer = false;
            textMesh.autoSizeTextContainer = true;
        }
    }
}