namespace Health {
    public interface IDamager {
        float Damage { get; }
    }
}