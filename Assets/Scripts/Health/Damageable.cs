using Events;
using UnityEngine;
using Utils;

namespace Health {
    [RequireComponent(typeof(Collider2D))]
    public class Damageable : MonoBehaviour {
        public LayerMask layerMask;
        public HealthComponent healthComponent;
        public float damageMultiplier = 1;
        public Vector3Event onHit;

        private void OnCollisionEnter2D(Collision2D other) {
            if (CollisionUtils.MaskCheck(other.gameObject.layer, layerMask) && healthComponent.Health > 0) {
                var damage = other.gameObject.GetComponent<IDamager>().Damage * damageMultiplier;
                onHit.Invoke(other.GetContact(0).point);
                if (damage > 0) {
                    healthComponent.Health -= damage;
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D other) {
            if (CollisionUtils.MaskCheck(other.gameObject.layer, layerMask) && healthComponent.Health > 0) {
                var damage = other.gameObject.GetComponent<IDamager>().Damage * damageMultiplier;
                onHit.Invoke(other.ClosestPoint(transform.position));
                if (damage > 0) {
                    healthComponent.Health -= damage;
                }
            }
        }
    }
}