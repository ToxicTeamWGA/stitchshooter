using UnityEngine;
using Utils;

namespace Health {
    [RequireComponent(typeof(Collider2D))]
    public class PlayerDamageable : MonoBehaviour {
        public LayerMask layerMask;
        public Player player;

        private void OnCollisionEnter2D(Collision2D other) {
            if (CollisionUtils.MaskCheck(other.gameObject.layer, layerMask)) {
                player.SpendHP(other.gameObject.GetComponent<IDamager>().Damage);
            }
        }

        private void OnTriggerEnter2D(Collider2D other) {
            if (CollisionUtils.MaskCheck(other.gameObject.layer, layerMask)) {
                player.SpendHP(other.gameObject.GetComponent<IDamager>().Damage);
            }
        }
    }
}