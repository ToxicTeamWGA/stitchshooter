using Attributes;
using Events;
using ScriptableObjects.Variables;
using UnityEngine;
using UnityEngine.Events;

namespace Health {
    public class HealthComponent : MonoBehaviour {
        public FloatEvent onDamage;
        public UnityEvent onDead;
        [Expandable] public FloatVariable maxHealth;

        public float MaxHealth => maxHealth.value;

        public float Health {
            get => _health;
            set {
                _health = Mathf.Min(value, maxHealth.value);

                if (_health <= 0) {
                    _health = Mathf.Max(value, 0);
                    onDead?.Invoke();
                }
                else {
                    onDamage?.Invoke(_health);
                }
            }
        }

        private float _health;

        private void Start() {
            _health = maxHealth.value;
        }
    }
}