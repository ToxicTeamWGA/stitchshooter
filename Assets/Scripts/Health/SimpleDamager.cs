using UnityEngine;

namespace Health {
    public class SimpleDamager: MonoBehaviour, IDamager {
        public float damage;

        public float Damage => damage;
    }
}