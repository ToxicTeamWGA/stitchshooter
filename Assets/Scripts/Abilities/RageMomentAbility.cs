namespace Abilities {
    public class RageMomentAbility : RageAbility {
        public float rageSpend;
        protected override void Activate() {
            base.Activate();
            rageSpendEvent.Raise(rageSpend);
        }
    }
}