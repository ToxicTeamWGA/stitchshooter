using ScriptableObjects.Events;
using ScriptableObjects.Variables;

namespace Abilities {
    public class HealAbility : HoldRageAbility {
        public FloatVariable hp;
        public FloatVariable maxHP;
        public FloatGameEvent healGainEvent;
        public FloatVariable maxRage;
        public float healPerSecond;

        protected override void HandleTick() {
            base.HandleTick();
            healGainEvent.Raise(healPerSecond * tickTime);
        }

        protected override bool CheckForTick() {
            return base.CheckForTick() && hp.value < maxHP.value && rage.value < maxRage.value && !isEnraged.value;
        }
    }
}