using Enemies;
using ScriptableObjects.Config.Enemy;
using UnityEngine;

namespace Abilities {
    public class CirclePushAbility : RageMomentAbility {
        [Header("CirclePushAbility")] public float radius;
        public float strength;
        public float stunDuration = 2f;
        public float xOffset;
        public float yOffset;
        public LayerMask pushLayers;
        public int maxEnemyToPush = 20;
        private Collider2D[] _results;

        protected override void Awake() {
            base.Awake();
            _results = new Collider2D[maxEnemyToPush];
        }

        protected override void Activate() {
            base.Activate();
            var transformPosition = transform.position;
            transformPosition = new Vector3(transformPosition.x + xOffset, transformPosition.y + yOffset);
            var size = Physics2D.OverlapCircleNonAlloc(transformPosition, radius, _results, pushLayers);
            for (var i = 0; i < size; i++) {
                var enemyCollider2D = _results[i];
                var enemy = enemyCollider2D.GetComponent<Enemy<EnemyConfig>>();
                if (enemy) {
                    enemy.Stun(stunDuration);
                }

                var direction = (enemyCollider2D.transform.position - transformPosition);
                var normalized = new Vector3(direction.normalized.x, direction.normalized.y);
                enemyCollider2D.attachedRigidbody.AddForce(strength * normalized, ForceMode2D.Impulse);
                Debug.Log("Push to: " + strength * normalized);
            }
        }
    }
}