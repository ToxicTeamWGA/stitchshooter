using UnityEngine;

namespace Abilities {
    public class HoldRageAbility : RageContinuousAbility {
        protected override void Activate() {
            isActive = true;
        }

        protected override void Update() {
            if (Input.GetButtonUp(inputName)) {
                Deactivate();
            }

            if (CheckForTick()) {
                time = 0;
                HandleTick();
            }

            if (Input.GetButtonDown(inputName) && CanActivate()) {
                Activate();
            }
        }
    }
}