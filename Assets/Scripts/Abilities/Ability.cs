using UnityEngine;

namespace Abilities {
    public abstract class Ability : MonoBehaviour {
        public string inputName;

        protected virtual void Update() {
            if (Input.GetButtonDown(inputName) && CanActivate()) {
                Activate();
            }
        }

        protected virtual void Awake() {
        }

        protected abstract bool CanActivate();

        protected abstract void Activate();
    }
}