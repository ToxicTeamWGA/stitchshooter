using ScriptableObjects.Variables;
using UnityEngine;

namespace Abilities {
    public abstract class RageContinuousAbility : RageAbility {
        public BoolVariable isEnraged;
        public float ragePerSecond;
        public float tickTime;
        protected bool isActive;
        protected float time;

        protected override void Awake() {
            base.Awake();
            time = tickTime;
        }

        protected override bool CanActivate() {
            return base.CanActivate() && !isActive;
        }

        protected override void Update() {
            if (Input.GetButtonDown(inputName)) {
                if (isActive) {
                    Deactivate();
                }
                else if (CanActivate()) {
                    Activate();
                }
            }

            if (CheckForTick()) {
                time = 0;
                HandleTick();
            }
        }

        protected virtual bool CheckForTick() {
            if (!isActive) return false;
            time += Time.unscaledDeltaTime;
            return time > tickTime;
        }

        protected virtual void HandleTick() {
            var spend = tickTime * ragePerSecond;
            if (rage.value >= spend) {
                rageSpendEvent.Raise(spend);
            }
            else {
                Deactivate();
            }
        }

        protected override void Activate() {
            base.Activate();
            isActive = true;
            isEnraged.value = true;
        }

        protected virtual void Deactivate() {
            isActive = false;
            isEnraged.value = false;
        }
    }
}