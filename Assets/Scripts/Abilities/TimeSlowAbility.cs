using UnityEngine;

namespace Abilities {
    public class TimeSlowAbility : RageContinuousAbility {
        public float slowdownFactor;

        protected override void Activate() {
            base.Activate();
            Debug.Log("Ability: " + typeof(TimeSlowAbility) + " activated");
            Time.timeScale = 1 / slowdownFactor;
            Time.fixedDeltaTime = Time.timeScale * .02f;
        }

        protected override void Deactivate() {
            base.Deactivate();
            Time.timeScale = 1f;
        }
    }
}