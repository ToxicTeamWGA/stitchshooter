using ScriptableObjects.Events;
using ScriptableObjects.Variables;
using UnityEngine;

namespace Abilities {
    public abstract class RageAbility : Ability {
        [Header("Rage Ability")] public FloatVariable rage;
        public FloatGameEvent rageSpendEvent;
        public float rageToActivate;

        protected override bool CanActivate() {
            return rage.value >= rageToActivate;
        }

        protected override void Activate() {
        }
    }
}